import serial
import time
import numpy as np
import math
import cv2
import matplotlib.pyplot as plt
import numpy as np
import PIL
import os


ser=serial.Serial('COM8') #ENTRAR SOMENTE 
ser.timeout=0.1 #Segundo
#ser.close()
time.sleep(1)
#ser.open()
ser.baudrate = 115200 #VELOCIDAD DEL PUERTO
print("PUERTO:", ser.name)

#VARIABLES
is_header=False
is_data_image=False
is_data_info=False
huffman_active=False
is_last_package=False
is_received_main_header=False
counter=b''
payload_size=144

#RELLENO
data_fill=np.zeros((1,144),'uint')
data_fill=data_fill.tolist()
data_fill=data_fill[0]

contador=0
contador_paquete=0
contador_bytes=0
primero_key=0
segundo_key=0
contador_key=0

archivo_aux=[]
archivo1=[]
archivo2=[]
arhcivo3=[]
archivo4=[]
arhcivo5=[]
file=1

def save_package_manager(data):
	f = open('dataManager'+'.pjt', 'wb')
	f.write(bytearray(data))
	f.close()


def create_file(data):
	ctn=0
	for i in range(0,len(data),3):
		print("Contador",i)
		ctn += 1
		tam_banda_real=data[i]
		tam_banda_real=tam_banda_real[0]
		tam_banda_recibida=len(data[i+2])
		print(tam_banda_real,tam_banda_recibida)
		print(type(tam_banda_real),type(tam_banda_recibida))
		
		f = open('headerData'+str(ctn)+'.pjt', 'wb')
		f.write(bytearray(data[i+1]))
		f.close()

		#if(tam_banda_real==tam_banda_recibida):			
		f2 = open('archivoH'+str(ctn)+'.pjt','wb')
		f2.write(bytearray(data[i+2]))
		f2.close()

def receive_data(ctn_data):

	#VARIABLES
	ser.flushInput()
	print("INICIANDO RECEPCION DE DATOS")

	package_lossBytes=[]	
	data_received=[]
	band_received=[]
	data_add=[]
	package_counter=0
	header_counter=0
	payload=b''
	max_package=0
	#CONTADOR DE PAQUETES PERDIDOS
	ctn_loss_package=0
	is_data_image_received=False

	while True:
		#time.sleep(1.5)	

		data_rec=ser.read(150)

		if(len(data_rec) != 0):				
			heder_data = data_rec[0:5]
			heder_data = heder_data.split(b';')
			payload = data_rec[6:]
			print("HEADER DATA: ",heder_data[0],heder_data[1],int.from_bytes(heder_data[2],'big'))
			is_complete_payload = (payload_size==len(payload) or max_package==(package_counter+1))
			#print(is_complete_payload)

			try:			
				if(heder_data[0] == b'n' and heder_data[1] == b's' and is_header_received):
					
					is_data_image_received=True
					package_counter += 1
					
					print(package_counter,int.from_bytes(heder_data[2],'big'),len(payload))

					if(package_counter == int.from_bytes(heder_data[2],'big')):

						band_received.extend(list(payload))

						if(int.from_bytes(heder_data[2],'big') == max_package):
							package_lossBytes.extend([package_counter,package_restante-len(payload)])
						else:
						 	package_lossBytes.extend([package_counter,0])
					else:
						res = (int.from_bytes(heder_data[2],'big')) - package_counter
						print("RESTA DE PAQUETES: ",res)

						for i in range(res):
							#print(i)
							package_lossBytes.extend([package_counter+i,144])

						package_counter = package_counter + res
						ctn_loss_package = ctn_loss_package + res

					header_counter = 0

				elif(heder_data[0] == b's' and heder_data[1] == b's'):

					header_counter += 1
					is_header_received = True
					is_data_image = True

					if(package_counter == max_package):
						print("SE TERMINO LA RECEPCIÓN DEL PAQUETE")
					else:
						print("FALTA PAQUETES EN LA BANDA")

					#Gettin band size
					Size_band=int.from_bytes(payload[11:13],'big')	
					Size_band=int(Size_band/8)

					# Getting data type(Huffman/no huffman)
					data_position_table=int.from_bytes(payload[13:14],'big')
					
					max_package = math.ceil(Size_band/payload_size)
					package_restante = round((Size_band/payload_size-int(Size_band/payload_size))*payload_size)
					
					band_position = int.from_bytes(heder_data[2],'big')

					print("TAMAÑO DE LA BANDA A RECIBIR : ",Size_band,header_counter)	
					if(header_counter == 1 and Size_band != 0):
						max_package = math.ceil(Size_band/payload_size)
						print("MAXIMA CANTIDAD DE PAQUETES: ",max_package)
						data_received.append(band_received)
						data_received.append([Size_band])
						data_received.append(list(payload))					
						print("TAMAÑO DEL RECIBIDO: ",len(band_received))
						band_received = []

					else:

						header_counter = header_counter - 1

					if(payload == b'footer' and is_data_image_received):
						data_received.append(band_received)
						data_received.append([Size_band])
						data_received.append(list(payload))					
						print("RECEPCIÓN FINALIZADA AL 100% : ",len(band_received))

						band_received = []
						break

					package_counter = 0

				elif(heder_data[0] == b'n' and heder_data[1] == b'n'):
					ctn_data += 1

					f2 = open('Datos/informacion'+str(ctn_data)+'.pjt','wb')
					f2.write(bytearray(list(payload)))
					f2.close()

					f2 = open('Mostrar/informacion.pjt','wb')
					f2.write(bytearray(list(payload)))
					f2.close()

				else:
					continue

			except Exception as e:
				print(e)
				continue


	print("CANTIDAD DE PAQUETES PERDIDOS: ",ctn_loss_package)

	return data_received[1:len(data_received)-2],ctn_data,package_lossBytes #RECORTE DE LA INFORMACION INPORTANTE
		
def execute_decoder(contador):
	[SubBandas,ListFe,ListRp,ListDesp,ListEsca,ListSizeBanda] = Desempaquev3N(5)
	BandaNivelado = Cuantiv3(SubBandas,ListFe,ListRp,ListDesp,ListEsca)

	[Y1,CO1,CG1] = Reconstruc(BandaNivelado,Nwav39bio)#AQUI SE PUEDE CAMBIAR CON Nwav35bio q es bior3.5, solo esta implemntado para 2 filtros 3.9 y 3.5
	[COI,CGI] = UpSampling(Y1,CO1,CG1)

	[M,N] = np.shape(Y1)
	G = np.zeros((M,N,3))
	Z = convertYtoRGB(Y1,COI,CGI,G)

	IMG = cv2.cvtColor(Z,cv2.COLOR_BGR2RGB)
	#cv2.imwrite("main.jpg", IMG)
	img_manager = str(contador)+".png"
	cv2.imwrite("Imagenes/img"+img_manager, IMG)
	cv2.imwrite("Mostrar/img.png", IMG)

	#ventana='main'
	#cv2.namedWindow(ventana, cv2.WINDOW_NORMAL)
	#cv2.imshow(ventana, IMG)
	#cv2.waitKey(0) 
	#cv2.destroyAllWindows() 
	
def main():
	print("Starting main")
	ctn = 0
	ctn_data = 0
	while True:
		try:
			ser.flushInput()
			ctn += 1
			print("=================Recibiendo datos================")
			data, ctn_data, package_lossBytes = receive_data(ctn_data)
	
			for dt in package_lossBytes:
				print(dt)

			#save_package_manager(package_lossBytes)
			create_file(data)
			#execute_decoder(ctn)
			#time.sleep(15)

		except Exception as e:
			print("Error en reconstruir la imagen")
			print("RX modo Sleep")
			print(e)
			continue

if __name__ == '__main__':
	main()


