import serial
import time
import numpy as np
#from ProcesosDecoderNH import *
import cv2
import matplotlib.pyplot as plt
import PIL
import os
import math
import threading
#threaded
from datetime import datetime

ser=serial.Serial('COM3') #ENTRAR SOMENTE 
ser.timeout=0.1 #Segundo
#ser.close()
time.sleep(1)
#ser.open()
ser.baudrate = 115200 #VELOCIDAD DEL PUERTO
print("PUERTO:", ser.name)

#VARIABLES
is_header=False
is_data_image=False
is_data_info=False
huffman_active=False
is_last_package=False
counter=b''
payload_size=144
is_last_package=False




#RELLENO
data_fill=np.zeros((1,144),'uint')
data_fill=data_fill.tolist()
data_fill=data_fill[0]

contador=0;
contador_paquete=0
contador_bytes=0
primero_key=0
segundo_key=0
contador_key=0



archivo_aux=[]
archivo1=[]
archivo2=[]
arhcivo3=[]
archivo4=[]
arhcivo5=[]
file=1

# CREANDO HILOS DE EJECUCION
#thead_data_manager = threaring.Thread(target=save_package_manager)

def save_package_manager(data,counter):
	f = open('dataManager'+str(counter)+'.pjt', 'wb')
	f.write(bytearray(data))
	f.close()

def save_main_package_manager(data):
	f = open('dataManager'+'.pjt', 'wb')
	f.write(bytearray(data))
	f.close()

def save_sensor_values(payload,counter):
	counter += 1
	f2 = open('Datos/informacion'+str(counter)+'.pjt','wb')
	f2.write(bytearray(list(payload)))
	f2.close()

	f2 = open('Mostrar/informacion.pjt','wb')
	f2.write(bytearray(list(payload)))
	f2.close()

def create_file(data,counter):
	tam_banda_real=data[0]
	tam_banda_real=tam_banda_real[0]
	tam_banda_recibida=len(data[2])
	#print(tam_banda_real,tam_banda_recibida)
	#print(type(tam_banda_real),type(tam_banda_recibida))
	
	f = open('headerData'+str(counter)+'.pjt', 'wb')
	f.write(bytearray(data[1]))
	f.close()

	f2 = open('archivoH'+str(counter)+'.pjt','wb')
	f2.write(bytearray(data[2]))
	f2.close()
	
def receive_data(ctn_data):
	#VARIABLES
	ser.flushInput()
	#ser.flush()	
	data_received=[]
	band_received=[]
	package_lossBytes=[]
	package_lossBytes_general=[]
	data_add=[]
	package_counter=0
	header_counter=0
	payload=b''
	max_package=0
	#CONTADOR DE PAQUETES PERDIDOS
	ctn_loss_package=0
	is_data_image_received=False
	band_counter = 0
	isPackageComplete=False
	isLastaPackageBand=False

	while True:
		#time.sleep(1.5)	

		data_rec=ser.read(150)

		if(len(data_rec)!=0):	

			heder_data=data_rec[0:5]
			heder_data=heder_data.split(b';')
			payload=data_rec[6:]
			#print("PAYLOAD TAMAÑO RECIBIDO: ",heder_data)
			is_complete_payload=(payload_size==len(payload) or max_package==(package_counter+1))
			#print(is_complete_payload)

			try:			
				if(heder_data[0]==b'n' and heder_data[1]==b's' and is_header_received and is_complete_payload):
					is_data_image_received=True
					
					package_counter += 1
					#15
					print(package_counter,int.from_bytes(heder_data[2],'big'),len(payload))
					     #(15,16)
					if(package_counter==int.from_bytes(heder_data[2],'big')):
						band_received.extend(list(payload))

						if(int.from_bytes(heder_data[2],'big')==max_package):
							package_lossBytes.extend([package_counter,package_restante-len(payload)])
						else:
							#print("RESTA BYTES: ",package_restante-len(payload))
							package_lossBytes.extend([package_counter,0])	

					else:
						res=(int.from_bytes(heder_data[2],'big'))-package_counter
						print("RESTA DE PAQUETES: ",res)
						#band_received.extend(data_fill*res)
						#band_received.extend(list(payload))
						for i in range(res):
							package_lossBytes.extend([package_counter+i,144])
						

						package_counter = package_counter + res

						if(package_counter==int.from_bytes(heder_data[2],'big')):
							band_received.extend(list(payload))
							if(int.from_bytes(heder_data[2],'big')==max_package):
								package_lossBytes.extend([package_counter,package_restante-len(payload)])
							else:
								#print("RESTA BYTES: ",package_restante-len(payload))
								package_lossBytes.extend([package_counter,0])	
						ctn_loss_package = ctn_loss_package + res

					header_counter=0

				elif(heder_data[0]==b's' and heder_data[1]==b's'):
					
					header_counter += 1
					is_header_received=True
					is_data_image=True
					print("-----------------------------------------------------------------")
					if(payload != b'footer'):
						payload=list(payload)
						band_position = payload[0]
						#print("POSICION DE LA BANDA :",band_position)
						#print("PAYLOAD : ",payload)
					else:
						print("Its time to sleep")

					if(package_counter != max_package and package_counter != 0 and is_data_image_received):
						is_data_image_received = False
						isLastaPackageBand = True
						res=abs(max_package-package_counter)
						print("RESTA DE PAQUETES: ",res)
						print(package_counter,max_package)
						for i in range(res):
							package_counter += 1
							if(package_counter==max_package):
								package_lossBytes.extend([package_counter,package_restante-len(payload)])
							else:
								package_lossBytes.extend([package_counter,144])

						ctn_loss_package += res
						data_received.append(band_received)
						package_lossBytes_general.extend(package_lossBytes)
						thead_data_manager = threading.Thread(target=save_package_manager, args=(package_lossBytes,band_position,))
						thead_data_manager.start()
						thead_data_bands = threading.Thread(target=create_file, args=(data_received,band_position,))
						thead_data_bands.start()

						if(band_position==5):
							thead__main_data_manager = threading.Thread(target=save_main_package_manager, args=(package_lossBytes_general,))
							thead__main_data_manager.start()
							package_lossBytes_general = []
							#isPackageComplete=True
							#package_lossBytes_general = []
							#band_counter = 0

						#print("TAMAÑO DEL RECIBIDO: ",len(band_received))
						package_lossBytes = []
						band_received = []
						data_received = []
						package_counter=0

					#band_number = int.from_bytes(payload[0:1],'big')
					#print("NUMERO DE BANDA RECIBIDO : ", banda)
					#print(payload)
					Size_band=int.from_bytes(payload[12:14],'big')	
					Size_band=int(Size_band/8)

					max_package=math.ceil(Size_band/payload_size)
					package_restante = round((Size_band/payload_size-int(Size_band/payload_size))*payload_size)

					#print("NUMERO DE BANDA RECIBIDO : ", band_position)
					#print("TAMAÑO DE LA BANDA A RECIBIR : ",Size_band,header_counter)	
					if(header_counter==1 and Size_band != 0):
						#band_counter += 1
						#print("NUMERO DE BANDA : ",band_position)
						max_package=math.ceil(Size_band/payload_size)
						#print("MAXIMA CANTIDAD DE PAQUETES: ",max_package)
						#data_received.append(band_received)
						data_received.append([Size_band])
						data_received.append(payload[1:])
						#print("HEADER : ",list(payload[1:]))			
						#print("TAMAÑO DEL RECIBIDO: ",len(band_received))
						band_received=[]
					else:
						header_counter = header_counter - 1

					if(payload==b'footer' and is_data_image_received):
						if(not isLastaPackageBand):
							is_data_image_received = False
							isLastaPackageBand = False
							data_received.append(band_received)
							package_lossBytes_general.extend(package_lossBytes)
							thead_data_manager = threading.Thread(target=save_package_manager, args=(package_lossBytes,band_position,))
							thead_data_manager.start()
							thead_data_bands = threading.Thread(target=create_file, args=(data_received,band_position,))
							thead_data_bands.start()

							if(band_position==5):
								thead__main_data_manager = threading.Thread(target=save_main_package_manager, args=(package_lossBytes_general,))
								thead__main_data_manager.start()
								package_lossBytes_general = []
								#package_lossBytes_general = []
								#isPackageComplete=True
								#package_lossBytes_general = []
								#band_counter = 0

							#print("TAMAÑO DEL RECIBIDO: ",len(band_received))
							package_lossBytes = []
							band_received = []
							data_received = []
							package_counter=0
				
				elif(heder_data[0]==b'n' and heder_data[1]==b'n'):
					ctn_data += 1
					thead_data_sensor = threading.Thread(target=save_sensor_values, args=(payload,ctn_data,))
					thead_data_sensor.start()
				
				else:
					continue

			except Exception as e:
				print(e)
				continue


	print("CANTIDAD DE PAQUETES PERDIDOS: ",ctn_loss_package)
	#return package_lossBytes,data_received[1:len(data_received)-2],ctn_data #RECORTE DE LA INFORMACION INPORTANTE
		
def execute_decoder(contador):
	[SubBandas,ListFe,ListRp,ListDesp,ListEsca,ListSizeBanda] = Desempaquev3N(5)
	BandaNivelado = Cuantiv3(SubBandas,ListFe,ListRp,ListDesp,ListEsca)

	[Y1,CO1,CG1] = Reconstruc(BandaNivelado,Nwav39bio)#AQUI SE PUEDE CAMBIAR CON Nwav35bio q es bior3.5, solo esta implemntado para 2 filtros 3.9 y 3.5
	[COI,CGI] = UpSampling(Y1,CO1,CG1)

	[M,N] = np.shape(Y1)
	G=np.zeros((M,N,3))
	Z=convertYtoRGB(Y1,COI,CGI,G)

	IMG=cv2.cvtColor(Z,cv2.COLOR_BGR2RGB)
	#cv2.imwrite("main.jpg", IMG)
	img_manager=str(contador)+".png"
	cv2.imwrite("Imagenes/img"+img_manager, IMG)
	cv2.imwrite("Mostrar/img.png", IMG)

	#ventana='main'
	#cv2.namedWindow(ventana, cv2.WINDOW_NORMAL)
	#cv2.imshow(ventana, IMG)
	#cv2.waitKey(0) 
	#cv2.destroyAllWindows() 

def main():	
	ctn=0
	ctn_data=0
	while True:
		try:
			ser.flushInput()
			ctn += 1
			print("=================Recibiendo datos================")
			receive_data(ctn_data)
			"""
			for dt in data:
				print(len(dt))
			for i in range(0,len(package_lossBytes),2):
				print(package_lossBytes[i],package_lossBytes[i+1])

			print("==============Sleeping================")
			
			for dt in data:
				print(len(dt))

			create_file(data)

			execute_decoder(ctn)
			time.sleep(15)
			"""
		except Exception as e:
			print("Error en reconstruir la imagen")
			print("RX modo Sleep")
			print(e)
			continue

if __name__ == '__main__':
	main()


