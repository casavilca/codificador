import tkinter as tk
import tkinter.ttk as ttk
from PIL import Image, ImageTk
import numpy as np
import cv2
import time
import threading


class App:
    def __init__(self, master):
        self.master = master
        self.master.title("Interfaz del Receptor")
        self.master.geometry("+50+50")
        self.master.configure(bg="#398AEC")

        frm = tk.Frame(self.master,bg="#398AEC")
        frm.pack(padx=10, pady=10)
                
        self.frm1 = tk.Frame(frm,bg="#398AEC")
        self.frm2 = tk.Frame(frm,bg="#398AEC")

        self.frm1.pack(side=tk.LEFT, anchor=tk.N)
        self.frm2.pack(side=tk.LEFT)
        
        self.frm1_1 = tk.LabelFrame(self.frm1, text="Fecha y Hora de Envío", width=600,bg="#398AEC",fg="#F4D03F",font='Helvetica 11 bold')
        self.frm1_2 = tk.LabelFrame(self.frm1, text="Barra de Carga",bg="#398AEC",fg="#F4D03F",font='Helvetica 11 bold')
        self.frm1_3 = tk.LabelFrame(self.frm1, text="Datos de Sensores",bg="#398AEC",fg="#F4D03F",font='Helvetica 11 bold')
        
        self.frm1_1.pack(padx=5, pady=5, fill=tk.X)
        self.frm1_2.pack(padx=5, pady=5, fill=tk.X)
        self.frm1_3.pack(padx=5, pady=5)
        
        self.frm2_1 = tk.LabelFrame(self.frm2, text="Imagen Reconstruida",bg="#398AEC",fg="#F4D03F",font='Helvetica 11 bold')
        self.frm2_1.pack(padx=5, pady=5)
        
        self.TextFecha = tk.StringVar()
        self.TextFecha.set("-------------------------")
        self.txEditFecha = ttk.Label(self.frm1_1,textvariable=self.TextFecha)
        self.txEditFecha.grid(row=0, column=1, padx=5, pady=5)



        self.lista = tk.Listbox(self.frm1_3)
        self.lista.insert(0, ["Sensor","01:","--->","-"])
        self.lista.insert(1, ["Sensor","02:","--->","-"])
        self.lista.insert(2, ["Sensor","03:","--->","-"])
        self.lista.insert(3, ["Sensor","04:","--->","-"])
        self.lista.insert(4, ["Sensor","05:","--->","-"])
        self.lista.insert(5, ["Sensor","06:","--->","-"])
        self.lista.insert(6, ["Sensor","07:","--->","-"])
        self.lista.insert(7, ["Sensor","08:","--->","-"])
        self.lista.insert(8, ["Sensor","09:","--->","-"])
        self.lista.insert(9, ["Sensor","10:","--->","-"])
        
        self.lista.place(x=100,y=200,width=200)
        self.lista.grid(row=0, column=1, padx=5, pady=5)

        self.canvasImg = tk.Canvas(self.frm2_1, height=600, width=120,bg="#54BF8D")
        self.canvasImg.pack(expand=True, fill=tk.BOTH)

        self.hilo = threading.Thread(target=self.Cambio, args=())
        self.hilo.start()   # Iniciamos la ejecución del thread,
        

    def LecturaDatos(self):

        f = open("Mostrar/informacion.pjt","rb")

        self.fechaS = str(f.read(24))
        self.fechaS = self.fechaS[1:len(self.fechaS)].split("'")[1]

        #acondicioanmiento FECHA
        # self.fechaF=''
        # for val in fechaS:
        #     if(val == ' '):
        #         self.fechaF = self.fechaF + '/'
        #     elif (val == ':'):
        #         self.fechaF = self.fechaF + '-'
        #     else:
        #         self.fechaF = self.fechaF + val

        self.datos = []
        #lee datos enteros 
        for linea in f.read():
            self.datos.append(linea)

        f.close()
        return self.fechaS,self.datos
    
    def Cambio(self):
        global new_pic
        
        while(True):
            #TIEMPO QUE DUERME EL HILO DE CAMBIO
            #duerme 1 minuto
            time.sleep(10)

            try:
                #actualiza datos (SENSORES)
                [Dfecha,Ddatos]= self.LecturaDatos()

                for i in range(len(Ddatos)):
                    if (i == 9):
                        print(i)
                        self.lista.delete(i)
                        self.lista.insert(i, ["Sensor",str(i+1)+":","--->",Ddatos[i]])
                    else:
                        print(i)
                        self.lista.delete(i)
                        self.lista.insert(i, ["Sensor","0"+str(i+1)+":","--->",Ddatos[i]])

                #coloca la fecha
                self.TextFecha.set(Dfecha)
            
                #actualiza imagen
                I1 = Image.open("Mostrar/img.png") #cambiar el nombre del archivo a conveniencia
                I1 = np.array(I1)
                resized = cv2.resize(I1,(93,564),interpolation=cv2.INTER_CUBIC)
                new_pic = ImageTk.PhotoImage(image=Image.fromarray(resized))
                my_image=self.canvasImg.create_image(60, 300, image=new_pic)

            except:
                pass


root = tk.Tk()
app = App(root)
root.mainloop()