from network import LoRa, WLAN , Bluetooth
import network
from machine import UART
import socket
import time

#VARIABLES
wlan1 = WLAN()
blt1 = Bluetooth()
server = network.Server()

wlan1.deinit()
blt1.deinit()
server.deinit()

uart1 = UART(1,115200)
uart1.init(115200, bits=8, parity=None, stop=1)
lora = LoRa(mode=LoRa.LORA,
            frequency=915000000,
            tx_power=18,
            bandwidth=LoRa.BW_500KHZ,
            sf=11,
            preamble=12,
            coding_rate=LoRa.CODING_4_7)
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)


s.setblocking(False)
print("Starting RX11")

while True:
    time.sleep(3)
    data=s.recv(150)
    #print(data)
    try:
        if(len(list(data))!=0):
            print("BYTES: ",len(list(data)))
            uart1.write(data)
            print(list(data[0:4]),":",data[0:14])
    except Exception as e:
        print(e)
        continue
