import serial
import time
import numpy as np
import math
import cv2
import matplotlib.pyplot as plt
import numpy as np
import PIL
import os

ser=serial.Serial('COM8') #ENTRAR SOMENTE 
ser.baudrate = 115200 #VELOCIDAD DEL PUERTO
ser.timeout=0.1 #Segundo
#ser.close()
time.sleep(1)
#ser.open()

print("PUERTO:", ser.name)

#VARIABLES
is_header=False
is_data_image=False
is_data_info=False
huffman_active=False
is_last_package=False
counter=b''
payload_size=144
is_last_package=False




#RELLENO
data_fill=np.zeros((1,144),'uint')
data_fill=data_fill.tolist()
data_fill=data_fill[0]

contador=0;
contador_paquete=0
contador_bytes=0
primero_key=0
segundo_key=0
contador_key=0



archivo_aux=[]
archivo1=[]
archivo2=[]
arhcivo3=[]
archivo4=[]
arhcivo5=[]
file=1

def save_package_manager(data):
	f = open('dataManager'+'.pjt', 'wb')
	f.write(bytearray(data))
	f.close()

def create_file(data):
	ctn=0
	for i in range(0,len(data),3):
		print("Contador",i)
		ctn += 1

		tam_banda_real=data[i]
		tam_banda_real=tam_banda_real[0]
		tam_banda_recibida=len(data[i+2])
		print(tam_banda_real,tam_banda_recibida)
		print(type(tam_banda_real),type(tam_banda_recibida))
		
		f = open('headerData'+str(ctn)+'.pjt', 'wb')
		f.write(bytearray(data[i+1]))
		f.close()

			
		f2 = open('archivoH'+str(ctn)+'.pjt','wb')
		f2.write(bytearray(data[i+2]))
		f2.close()

		
def receive_data(ctn_data):
	#VARIABLES
	ser.flushInput()
	print("INICIANDO RECEPCION DE DATOS")
	tam_actual=0
	package_lossBytes=[]
	package_restante=0
	max_package=0

	data_received=[]
	band_received=[]
	data_add=[]
	is_huffman=False
	package_counter=0
	header_counter=0
	payload=b''
	
	is_waiting_reSend=True

	footer_counter=0
	#CONTADOR DE PAQUETES PERDIDOS
	ctn_loss_package=0
	is_data_image_received=False
	is_header_received= False

	while True:
		#time.sleep(1.5)		
		ser.flush()
		
		try:
			data_rec=ser.read(150)

			if(len(data_rec)!=0):
				heder_data=data_rec[0:5]
				heder_data=heder_data.split(b';')

				payload=data_rec[6:]
				#print("PAYLOAD TAMAÑO RECIBIDO: ",len(payload))
				is_complete_payload=(payload_size==len(payload) or max_package==(package_counter+1))
				if(heder_data[0]==b'n' and heder_data[1]==b's' and is_header_received and is_complete_payload):
					
					is_data_image_received=True
					package_counter += 1
					
					#print(len(data_rec),int.from_bytes(heder_data[2],'big'),data_rec[0:10])
					print(package_counter,int.from_bytes(heder_data[2],'big'))

					if(package_counter==int.from_bytes(heder_data[2],'big')):
						band_received.extend(list(payload))
						#package_lossBytes.extend([package_counter,0])

						if(int.from_bytes(heder_data[2],'big')==max_package):
							package_lossBytes.extend([package_counter,package_restante-len(payload)])
						else:
						 	package_lossBytes.extend([package_counter,0])

					else:			
						res=(int.from_bytes(heder_data[2],'big'))-package_counter

						for i in range(res):
						
							package_lossBytes.extend([package_counter+i,144])

						package_counter += res
						ctn_loss_package += res

					header_counter=0

				elif(heder_data[0]==b's' and heder_data[1]==b's'):
					
					is_waiting_reSend=False
					header_counter += 1
					is_header=True
					is_data_image=True
					is_header_received=True

					if(package_counter != max_package and package_counter != 0):
						res=max_package-package_counter
						for i in range(res):
							package_counter += 1
							
							if(package_counter==max_package):
								package_lossBytes.extend([package_counter,package_restante-len(payload)])
							else:
								package_lossBytes.extend([package_counter,144])

						ctn_loss_package += res

					#Gettin band size
					Size_band=int.from_bytes(payload[11:13],'big')	
					Size_band=int(Size_band/8)

					max_package=math.ceil(Size_band/payload_size)
					package_restante = round((Size_band/payload_size-int(Size_band/payload_size))*payload_size)
					#print("=========================================")
					print("INFO: ",max_package,package_restante)
					
					data_position_table=int.from_bytes(payload[13:14],'big')
					
					

					=int.from_bytes(heder_data[2],'big')

					print("TAMAÑO DE LA BANDA A RECIBIR : ",Size_band)	
					if(header_counter==1 and Size_band != 0):
						max_package=math.ceil(Size_band/payload_size)
						print("MAXIMA CANTIDAD DE PAQUETES: ",max_package)
						data_received.append(band_received)
						data_received.append([Size_band])
						data_received.append(list(payload))					
						print("TAMAÑO DEL RECIBIDO: ",len(band_received))
						band_received=[]
					else:
						header_counter = header_counter - 1
						
					if(payload==b'footer' and is_data_image_received):
						data_received.append(band_received)
						data_received.append([Size_band])
						data_received.append(list(payload))					
						print("TAMAÑO DEL RECIBIDO: ",len(band_received))
						band_received=[]
						#print("We are in footer")
						break
						#return package_lossBytes,data_received[1:(len(data_received)-2)] #RECORTE DE LA INFORMACION INPORTANTE

					#REINICIANDO VARIABLES
					#band_received=[]
					package_counter=0

				elif(heder_data[0] == b'n' and heder_data[1] == b'n'):
					
					ctn_data += 1

					f2 = open('Datos/informacion'+str(ctn_data)+'.pjt','wb')
					f2.write(bytearray(list(payload)))
					f2.close()

					f2 = open('Mostrar/informacion.pjt','wb')
					f2.write(bytearray(list(payload)))
					f2.close()

		except Exception as e:
				print(e)
				continue
	#print("CANTIDAD DE PAQUETES PERDIDOS: ",ctn_loss_package)
	return package_lossBytes,data_received[1:(len(data_received)-2)],ctn_data#RECORTE DE LA INFORMACION INPORTANTE
	#return True,data_received #RECORTE DE LA INFORMACION INPORTANTE
		



def main():
	ctn_data = 0
	while True:
		try:
			ser.flushInput()

			package_lossBytes,data,ctn_data=receive_data(ctn_data)
			create_file(data)
			save_package_manager(package_lossBytes)

			for dt in data:
				print(len(dt))
			

			for i in range(0,len(package_lossBytes),2):
				print(package_lossBytes[i],package_lossBytes[i+1])

			print("==============Sleeping================")
			#time.sleep(2)
			#print("================I wake up==================")

		except Exception as e:
			print("Error en reconstruir la imagen")
			print("RX modo Sleep")
			print(e)
			continue

if __name__ == '__main__':
	main()

