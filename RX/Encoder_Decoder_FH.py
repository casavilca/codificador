
from ProcesosFH import *
import cv2
import matplotlib.pyplot as plt

Irgb = cv2.imread('blanco1-ultimo.png')
I1 = cv2.cvtColor(Irgb,cv2.COLOR_BGR2RGB)

[IY,ICB,ICR]       = SubSampling(I1)
[YA2,CBA2,CRA2]    = Descom(IY,ICB,ICR,Nwav35bio)
[bcompri,fe,RP]    = Recuan(YA2,CBA2,CRA2,6,6,6)
[frec,xval]        = Peso(bcompri) #frecuencia de cada pixel
[SalidaBits,Dic]   = EmpaqueH(frec,xval,bcompri,fe,RP) #empaquetado con Huffman
#print((1504*248*3*8)/SalidaBits)



#PERTENECE A LA PARTE DE DECODIFICACION DE AMBOS BLOQUES

# [arreBanda,arrefe,arrerp] = DesempaqueH(3,xval,Dic)#desempaquetado con Huffman

# BandaNivelado   = Cuanti(arreBanda,arrefe,arrerp)
# [Y1,CB1,CR1]    = Reconstruc(BandaNivelado,Nwav35bio)
# [CBI,CRI]       = UpSampling(Y1,CB1,CR1)

# [M,N,P]=np.shape(Irgb)
# G=np.zeros((M,N,P))

# Z=convertYtoRGB(Y1,CBI,CRI,G)

# plt.figure(1)
# plt.imshow(Z)

plt.figure(2)
plt.imshow(I1)
plt.show()



