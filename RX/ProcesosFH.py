import cv2
import numpy as np
import pywt
from math import ceil
from time import time
import huffman

class Bio35FilterBank(object):
    @property
    def filter_bank(self):

        cdl1 = -0.0048828125
        cdl2 =  0.0146484375
        cdl3 =  0.0185546875
        cdl4 = -0.0947265625
        cdl5 = -0.0253906250
        cdl6 =  0.3417968750
        cdl7 =  0.3417968750
        cdl8 = -0.0253906250
        cdl9 = -0.0947265625
        cdl10=  0.0185546875
        cdl11=  0.0146484375
        cdl12= -0.0048828125

        cdh1 =  0
        cdh2 =  0
        cdh3 =  0
        cdh4 =  0
        cdh5 = -0.1250
        cdh6 =  0.3750
        cdh7 = -0.3750
        cdh8 =  0.1250
        cdh9 =  0
        cdh10 = 0
        cdh11 = 0
        cdh12 = 0

        crl1 = 0
        crl2 = 0
        crl3 = 0
        crl4 = 0
        crl5 = 0.5
        crl6 = 1.5
        crl7 = 1.5
        crl8 = 0.5
        crl9 = 0
        crl10= 0
        crl11= 0
        crl12= 0

        crh1 =  -0.01953125
        crh2 =  -0.05859375
        crh3 =   0.07421875
        crh4 =   0.37890625
        crh5 =  -0.10156250
        crh6 =  -1.36718750
        crh7 =   1.36718750
        crh8 =   0.10156250
        crh9 =  -0.37890625
        crh10 = -0.07421875
        crh11 =  0.05859375
        crh12 =  0.01953125

        dec_lo = [cdl1,cdl2,cdl3,cdl4,cdl5,cdl6,cdl7,cdl8,cdl9,cdl10,cdl11,cdl12]
        dec_hi = [cdh1,cdh2,cdh3,cdh4,cdh5,cdh6,cdh7,cdh8,cdh9,cdh10,cdh11,cdh12]
        rec_lo = [crl1,crl2,crl3,crl4,crl5,crl6,crl7,crl8,crl9,crl10,crl11,crl12]
        rec_hi = [crh1,crh2,crh3,crh4,crh5,crh6,crh7,crh8,crh9,crh10,crh11,crh12]
        
        return [dec_lo, dec_hi, rec_lo, rec_hi]
filter_bank = Bio35FilterBank()
Nwav35bio = pywt.Wavelet(name="myWavelet35", filter_bank=filter_bank)

def UEntero8(A):
    [f,c]=np.shape(A)

    for i in range(0,f):
        for j in range(0,c):
            if A[i][j]<0:
                A[i][j]=0
            elif A[i][j]>255:
                A[i][j]=255
        
    return A

def SubSampling(Irgb):

    #conversion de rgb a ycbcr(CORRECTO)
    Irgb = np.array(Irgb)

    R=Irgb[:,:,0]
    G=Irgb[:,:,1]
    B=Irgb[:,:,2]

    #convert rgb to ycbcr(CORRECTO)
    IY  = 0.257*R + 0.504*G + 0.098*B +16
    ICB = -0.148*R - 0.291*G + 0.439*B +128
    ICR = 0.439*R - 0.368*G - 0.071*B +128

    #round of component ycbcr(CORRECTO)
    IY = np.round(IY)
    ICB = np.round(ICB)
    ICR = np.round(ICR)

    
    #formato 420
    ICR=ICR[::2, ::2]
    ICB=ICB[::2, ::2]
    
    return IY,ICB,ICR

def Descom(IY,ICB,ICR,wavelet_nor):

    # Y luminancia
    YA1, (YH1, YV1, YD1) = pywt.dwt2(IY,wavelet_nor,'periodization')
    YA2, (YH2, YV2, YD2) = pywt.dwt2(YA1,wavelet_nor,'periodization')
    # CB luminancia
    CBA1, (CBH1, CBV1, CBD1) = pywt.dwt2(ICB,wavelet_nor,'periodization')
    CBA2, (CBH2, CBV2, CBD2) = pywt.dwt2(CBA1,wavelet_nor,'periodization')
    # CR luminancia
    CRA1, (CRH1, CRV1, CRD1) = pywt.dwt2(ICR,wavelet_nor,'periodization')
    CRA2, (CRH2, CRV2, CRD2) = pywt.dwt2(CRA1,wavelet_nor,'periodization')

    return YA2,CBA2,CRA2

def Recuan(YA2,CBA2,CRA2,rpy,rpb,rpr):

    rpycbcr= [rpy,rpb,rpr]
    bandas = [YA2,CBA2,CRA2]
    [F] = np.shape(bandas)
    
    RP=[]
    fe=[]
    bcompri=[]

    for i in range(F):
        fe.append(np.max(np.max(np.abs(bandas[i]))))

        #aprox lumininacia
        RP.append(rpycbcr[i])
        val_ecu=(pow(2,(rpycbcr[i]-1))-1)/fe[i]
        bcompri.append(np.round(bandas[i]*val_ecu))
    
    return bcompri,fe,RP

def UnionBits(m,Mx,Nx,fex,rpx):
    #matriz
    m = np.round(m)
    m = m.astype(int)
    #array 1 fila
    m = m.flatten()

    #convert all int to one  string
    Stbin = ''
    for val in m:
        Stbin = Stbin + ('{0:06b}'.format(val))
    
    #agregando cabezera
    Mx  = '{0:016b}'.format(Mx)
    Nx  = '{0:016b}'.format(Nx)
    fex = '{0:08b}'.format(fex)
    rpx = '{0:08b}'.format(rpx)

    Stbin = Mx + Nx + fex + rpx + Stbin

    #tomar 8 en 8 and convert to int
    Lintu = []
    for val2 in range(int(len(Stbin)/8)):
        Lintu.append(int(Stbin[8*val2:8*val2+8],2))

    #salida de lista de enteros y los bits totales de envio de cada matriz
    
    return Lintu,Stbin,

#FUNCIONES HUFFMAN

#caculo de las cantidades que hay por cada pixel
def Peso(bandas_comprimidas):
    frec = [[],[],[]]
    xval = [[],[],[]]

    for i in range(len(bandas_comprimidas)):
        banda = bandas_comprimidas[i]
        xmi = np.min(np.min(banda))
        xma = np.max(np.max(banda))
        
        for j in range(int(xmi),int(xma)+1):
            cantidad = np.count_nonzero(banda == j)

            if cantidad>0:
                frec[i].append(cantidad)
                xval[i].append(j)
    
    return frec,xval  

#crea el diccionario y agrupa los bits a enviar
def GrupBitsH(frec,xval,subM,fex,rpx):

    #armar el diccionario
    cbook=[]
    for k in range(len(frec)):
        cbook.append((xval[k],frec[k]))

    diccionario = huffman.codebook(cbook)

    #armar LOS BITS
    [f,c]=np.shape(subM)

    Bagrup = ''
    for i in range(f):
        for j in range(c):
            #print(int(subM[i,j]))
            Bagrup = Bagrup + diccionario[subM[i,j]]

    #agregando cabezera
    Mx  = '{0:016b}'.format(f)
    Nx  = '{0:016b}'.format(c)
    fex = '{0:08b}'.format(fex)
    rpx = '{0:08b}'.format(rpx)

    #print(len(Mx+Nx+fex+rpx+Bagrup))

    if len(Bagrup)%8 == 0:
        Bagrup = '{0:08b}'.format(0) + Mx + Nx + fex + rpx + Bagrup

    else:
        NBitsFaltan = (8*((len(Bagrup)//8)+1) - len(Bagrup))
        #print(NBitsFaltan)

        BitsFaltan = ''
        for agrega in range(NBitsFaltan):
            BitsFaltan = BitsFaltan + '1'

        #print(BitsFaltan)
        Bagrup =  '{0:08b}'.format(NBitsFaltan) + BitsFaltan + Mx + Nx + fex + rpx + Bagrup

    #print(len(Bagrup))

    #tomar 8 en 8 and convert to int
    LintuH = []
    for val2 in range(int(len(Bagrup)/8)):
        LintuH.append(int(Bagrup[8*val2:8*val2+8],2))
    
    TamBagroup = len(Bagrup)
            
    return LintuH,TamBagroup,diccionario

def EmpaqueH(frec,xval,bcompri,fe,RP):
    
    bitsOut = 0
    Dic =[]
    for i in range(len(bcompri)):

        f = open('archivoH'+str(i+1)+'.pjt', 'wb')

        [LintuH,TamBagroup,diccionario] = GrupBitsH(frec[i],xval[i],bcompri[i],int(round(fe[i])),RP[i])
        #print(TamBagroup)
        f.write(bytearray(LintuH))# matriz
        f.close()

        bitsOut = bitsOut + TamBagroup

        Dic.append(diccionario)
    
    return bitsOut,Dic

def DesempaqueH(numBandas,xval,Dicti):

    arreBanda= []
    arrefe   = []
    arrerp   = []

    for i in range(numBandas):
        f = open('archivoH'+str(i+1)+'.pjt', 'rb')

        NBex = int.from_bytes(f.read(1), "big")

        beta = ''
        for j in f.read():
            beta = beta + '{0:08b}'.format(j)

        f.close()

        #elimina los bits agregados
        beta = beta[NBex:len(beta)]

        #obtener cabezera
        Mx  = int(beta[0:16],2)
        beta = beta[16:len(beta)]

        Nx  = int(beta[0:16],2)
        beta = beta[16:len(beta)]

        fex = int(beta[0:8],2)
        beta = beta[8:len(beta)]

        rpx = int(beta[0:8],2)
        beta = beta[8:len(beta)]


        LiDic = []
        while len(beta)>0:

            for k in range(len(xval[i])):
                Bdic = Dicti[i][xval[i][k]]
                if Bdic == beta[0:len(Bdic)]:    
            
                    LiDic.append(xval[i][k])
                    beta = beta.replace(Bdic,'',1)
        
        #reacondicionar
        Lrp = np.array(LiDic)
        Lrp = np.reshape(Lrp,(Mx,Nx))
        
        #
        arreBanda.append(Lrp)
        arrefe.append(fex)
        arrerp.append(rpx)
    
    return arreBanda,arrefe,arrerp


def Cuanti(bcompri,fe,RP):
    
    [F] = np.shape(bcompri)
    
    BandaNivelado = []

    for i in range(F):
        val_ecu = (fe[i])/(pow(2,(RP[i]-1))-1)
        BandaNivelado.append(bcompri[i]*val_ecu)

    return BandaNivelado

def Reconstruc(BandaNivelado,wavelet_nor):

    
    Y2  = pywt.idwt2((BandaNivelado[0],(None,None,None)),wavelet_nor,'periodization')
    Y1  = pywt.idwt2((Y2,(None,None,None)),wavelet_nor,'periodization')

    CB2 = pywt.idwt2((BandaNivelado[1],(None,None,None)),wavelet_nor,'periodization')
    CB1 = pywt.idwt2((CB2,(None,None,None)),wavelet_nor,'periodization')

    CR2 = pywt.idwt2((BandaNivelado[2],(None,None,None)),wavelet_nor,'periodization')
    CR1 = pywt.idwt2((CR2,(None,None,None)),wavelet_nor,'periodization')

    #valminY=np.min(np.min(Y1))
    #valminCB=np.min(np.min(CB1))
    #valminCR=np.min(np.min(CR1))

    #valmaxY=np.max(np.max(Y1))
    #valmaxCB=np.max(np.max(CB1))
    #valmaxCR=np.max(np.max(CR1))


    #Y1=((Y1+abs(valminY))*255)/(valmaxY+abs(valminY))
    #CB1=((CB1+abs(valminCB))*255)/(valmaxCB+abs(valminCB))
    #CR1=((CR1+abs(valminCR))*255)/(valmaxCR+abs(valminCR))


    return Y1,CB1,CR1

def UpSampling(Y1,CB1,CR1):
    #formato 420
    #upsampling orden 0
    [tf,tc] = Y1.shape
    CBI = np.zeros((tf,tc))
    CRI = np.zeros((tf,tc))

    for f in range(0,ceil(tf/2)):
        for c in range(0,ceil(tc/2)):
            CBI[2*f,2*c] = CB1[f,c]
    
    CBI[:,1::2] = CBI[:,0::2]
    CBI[1::2,:]  = CBI[0::2,:]

    for f in range(0,ceil(tf/2)):
        for c in range(0,ceil(tc/2)):
            CRI[2*f,2*c] = CR1[f,c]
    
    CRI[:,1::2] = CRI[:,0::2]
    CRI[1::2,:]  = CRI[0::2,:]

    return CBI,CRI
    
def convertYtoRGB(Y1,CB1,CR1,Z):

    R1=1.164*(Y1-16) + 1.596*(CR1-128)
    G1=1.164*(Y1-16) - 0.813*(CR1-128) - 0.391*(CB1-128)
    B1=1.164*(Y1-16) + 2.018*(CB1-128)


    R1 = np.round(R1)
    G1 = np.round(G1)
    B1 = np.round(B1)

    R1=UEntero8(R1)
    G1=UEntero8(G1)
    B1=UEntero8(B1)
    
    Z[:,:,0]=R1
    Z[:,:,1]=G1
    Z[:,:,2]=B1
    Z=np.uint8(Z)

    return Z
