import cv2
import numpy as np
import pywt
from math import ceil
from time import time
import huffman

class Bio35FilterBank(object):
    @property
    def filter_bank(self):

        cdl1 = -0.0048828125
        cdl2 =  0.0146484375
        cdl3 =  0.0185546875
        cdl4 = -0.0947265625
        cdl5 = -0.0253906250
        cdl6 =  0.3417968750
        cdl7 =  0.3417968750
        cdl8 = -0.0253906250
        cdl9 = -0.0947265625
        cdl10=  0.0185546875
        cdl11=  0.0146484375
        cdl12= -0.0048828125

        cdh1 =  0
        cdh2 =  0
        cdh3 =  0
        cdh4 =  0
        cdh5 = -0.1250
        cdh6 =  0.3750
        cdh7 = -0.3750
        cdh8 =  0.1250
        cdh9 =  0
        cdh10 = 0
        cdh11 = 0
        cdh12 = 0

        crl1 = 0
        crl2 = 0
        crl3 = 0
        crl4 = 0
        crl5 = 0.5
        crl6 = 1.5
        crl7 = 1.5
        crl8 = 0.5
        crl9 = 0
        crl10= 0
        crl11= 0
        crl12= 0

        crh1 =  -0.01953125
        crh2 =  -0.05859375
        crh3 =   0.07421875
        crh4 =   0.37890625
        crh5 =  -0.10156250
        crh6 =  -1.36718750
        crh7 =   1.36718750
        crh8 =   0.10156250
        crh9 =  -0.37890625
        crh10 = -0.07421875
        crh11 =  0.05859375
        crh12 =  0.01953125

        dec_lo = [cdl1,cdl2,cdl3,cdl4,cdl5,cdl6,cdl7,cdl8,cdl9,cdl10,cdl11,cdl12]
        dec_hi = [cdh1,cdh2,cdh3,cdh4,cdh5,cdh6,cdh7,cdh8,cdh9,cdh10,cdh11,cdh12]
        rec_lo = [crl1,crl2,crl3,crl4,crl5,crl6,crl7,crl8,crl9,crl10,crl11,crl12]
        rec_hi = [crh1,crh2,crh3,crh4,crh5,crh6,crh7,crh8,crh9,crh10,crh11,crh12]
        
        return [dec_lo, dec_hi, rec_lo, rec_hi]
filter_bank = Bio35FilterBank()
Nwav35bio = pywt.Wavelet(name="myWavelet35", filter_bank=filter_bank)

def UEntero8(A):
    [f,c]=np.shape(A)

    for i in range(0,f):
        for j in range(0,c):
            if A[i][j]<0:
                A[i][j]=0
            elif A[i][j]>255:
                A[i][j]=255
        
    return A

def RGBtoYCOCG(I1):

    Irgb = np.array(I1)

    R=Irgb[:,:,0]
    G=Irgb[:,:,1]
    B=Irgb[:,:,2]
    
    Iy  =  0.25*R + 0.50*G + 0.25*B
    ICo =  0.50*R - 0.50*B
    ICg = -0.25*R + 0.50*G - 0.25*B

    return Iy,ICo,ICg

def SubSampling(ICo,ICg):

    #round of component ycbcr(CORRECTO)

    #ICo = np.round(ICo)
    #ICg = np.round(ICg)

    #formato 420
    ICo=ICo[::2, ::2]
    ICg=ICg[::2, ::2]
    
    return ICo,ICg


def Descom(Iy,ICo,ICg,wavelet_nor):

    # Y luminancia
    Ya1, (YH1, YV1, YD1) = pywt.dwt2(Iy,wavelet_nor,'periodization')
    Ya2, (YH2, YV2, YD2) = pywt.dwt2(Ya1,wavelet_nor,'periodization')
    Ya3, (YH3, YV3, YD3) = pywt.dwt2(Ya2,wavelet_nor,'periodization')
    # Co luminancia
    CoA1, (CoH1, CoV1, CoD1) = pywt.dwt2(ICo,wavelet_nor,'periodization')
    CoA2, (CoH2, CoV2, CoD2) = pywt.dwt2(CoA1,wavelet_nor,'periodization')
    CoA3, (CoH3, CoV3, CoD3) = pywt.dwt2(CoA2,wavelet_nor,'periodization')
    CoA4, (CoH4, CoV4, CoD4) = pywt.dwt2(CoA3,wavelet_nor,'periodization')
    # Cg luminancia
    CgA1, (CgH1, CgV1, CgD1) = pywt.dwt2(ICg,wavelet_nor,'periodization')
    CgA2, (CgH2, CgV2, CgD2) = pywt.dwt2(CgA1,wavelet_nor,'periodization')
    CgA3, (CgH3, CgV3, CgD3) = pywt.dwt2(CgA2,wavelet_nor,'periodization')
    CgA4, (CgH4, CgV4, CgD4) = pywt.dwt2(CgA3,wavelet_nor,'periodization')

    return Ya3,YH3,YV3,CoA4,CgA4

def Recuan(YA,YH3,YV3,CAo,CAg,rpy,rpb,rpr):
    
    CAo=CAo
    CAg=CAg

    rpycbcr= [rpy,rpy,rpy,rpb,rpr]
    bandas = [YA,YH3,YV3,CAo,CAg]
    [F] = np.shape(bandas)
    
    RP=[]
    fe=[]
    bcompri=[]

    
    for i in range(F):
        fe.append(np.max(np.max(np.abs(bandas[i]))))

        #aprox lumininacia
        RP.append(rpycbcr[i])
        val_ecu=(pow(2,(rpycbcr[i]-1))-1)/fe[i]
        bcompri.append(np.round(bandas[i]*val_ecu))
    
    return bcompri,fe,RP

def UnionBits(m,Mx,Nx,fex,rpx):
    #matriz
    m = np.round(m)
    m = m.astype(int)
    #array 1 fila
    m = m.flatten()

    #convert all int to one  string
    Stbin = ''
    for val in m:
        Stbin = Stbin + ('{0:06b}'.format(val))
    
    #agregando cabezera
    Mx  = '{0:016b}'.format(Mx)
    Nx  = '{0:016b}'.format(Nx)
    fex = '{0:08b}'.format(fex)
    rpx = '{0:08b}'.format(rpx)

    Stbin = Mx + Nx + fex + rpx + Stbin

    #tomar 8 en 8 and convert to int
    Lintu = []
    for val2 in range(int(len(Stbin)/8)):
        Lintu.append(int(Stbin[8*val2:8*val2+8],2))

    #salida de lista de enteros y los bits totales de envio de cada matriz
    
    return Lintu,Stbin,

#FUNCIONES HUFFMAN

#caculo de las cantidades que hay por cada pixel
def Peso(bandas_comprimidas):
    frec = [[],[],[],[],[]]
    xval = [[],[],[],[],[]]

    for i in range(len(bandas_comprimidas)):
        banda = bandas_comprimidas[i]
        xmi = np.min(np.min(banda))
        xma = np.max(np.max(banda))
        
        for j in range(int(xmi),int(xma)+1):
            cantidad = np.count_nonzero(banda == j)

            if cantidad>0:
                frec[i].append(cantidad)
                xval[i].append(j)
    
    return frec,xval  

#crea el diccionario y agrupa los bits a enviar
def GrupBitsH(frec,xval,subM,fex,rpx):
 
    #armar el diccionario
    cbook=[]
    for k in range(len(frec)):#--------------REVISAR EL LEN
        cbook.append((xval[k],frec[k]))

    #se tiene el diccionario-relacion pixel a bits
    diccionario = huffman.codebook(cbook)

    #armar LOS BITS
    [f,c]=np.shape(subM)

    Bagrup = ''
    for i in range(f):
        for j in range(c):
            #print(int(subM[i,j]))
            #busca el valor del pixel en el diccionario y devuelve el valor en bits
            #se junto los bits 1 en 1 q va de columna en columna luego fila
            Bagrup = Bagrup + diccionario[subM[i,j]]

    #agregando cabezera
    Mx  = '{0:016b}'.format(f)
    Nx  = '{0:016b}'.format(c)
    fex = '{0:08b}'.format(fex)
    rpx = '{0:08b}'.format(rpx)

    #print(len(Mx+Nx+fex+rpx+Bagrup))


    #si es divisible entre 8 no se agrega una cabezera de bits para completar
    #los bits al inicio siempre se agregan asi sea divisible o no
    if len(Bagrup)%8 == 0:
        Bagrup = '{0:08b}'.format(0) + Mx + Nx + fex + rpx + Bagrup
        print('entro')

    else:
        NBitsFaltan = (8*((len(Bagrup)//8)+1) - len(Bagrup))
        #print(NBitsFaltan)

        BitsFaltan = ''
        for agrega in range(NBitsFaltan):
            BitsFaltan = BitsFaltan + '1'

        #print(BitsFaltan)
        Bagrup =  '{0:08b}'.format(NBitsFaltan) + BitsFaltan + Mx + Nx + fex + rpx + Bagrup

    #print(len(Bagrup))

    #se guarda el tamaño de bits agrupados total
    TamBagroup = len(Bagrup)

    #tomar 8 en 8 and convert to int de los bits agrupados
    #se guardan todos los enteros en una lista LintuH
    LintuH = []
    for val2 in range(int(len(Bagrup)/8)):
        LintuH.append(int(Bagrup[8*val2:8*val2+8],2))
    
            
    return LintuH,TamBagroup,diccionario

def EmpaqueH(frec,xval,bcompri,fe,RP):
    
    bitsOut = 0
    Dic =[]
    for i in range(len(bcompri)):

        f = open('archivoNH'+str(i+1)+'.kv', 'wb')

        [LintuH,TamBagroup,diccionario] = GrupBitsH(frec[i],xval[i],bcompri[i],int(round(fe[i])),RP[i])
        #print(TamBagroup)
        f.write(bytearray(LintuH))# matriz
        f.close()

        bitsOut = bitsOut + TamBagroup

        Dic.append(diccionario)
    
    return bitsOut,Dic

def DesempaqueH(numBandas,xval,Dicti):

    arreBanda= []
    arrefe   = []
    arrerp   = []

    for i in range(numBandas):
        f = open('archivoNH'+str(i+1)+'.kv', 'rb')

        #los primeros 8bits indican cuantos bits se agregan
        NBex = int.from_bytes(f.read(1), "big")

        #lectura de todos los bits siguentes
        #se almacenan en beta como beta='10101010101011' sin los 8bits de NBex
        beta = ''
        for j in f.read():
            beta = beta + '{0:08b}'.format(j)

        f.close()
        
        #elimina los bits agregados 
        beta = beta[NBex:len(beta)]

        #obtener cabezera
        Mx  = int(beta[0:16],2)
        beta = beta[16:len(beta)]

        Nx  = int(beta[0:16],2)
        beta = beta[16:len(beta)]

        fex = int(beta[0:8],2)
        beta = beta[8:len(beta)]
        
        rpx = int(beta[0:8],2)
        beta = beta[8:len(beta)]

        #los bits de beta que quedan son de la matriz-subbanda
        #
        LiDic = []
        while len(beta)>0:

            for k in range(len(xval[i])):
                #se obtiene el segmento de bits respecto a valores entero del diccionario 
                Bdic = Dicti[i][xval[i][k]]
                #si el valor de bits Bdic es = a los bits del desempaquetado
                #se alamacena el valor entero realcionado al bit en el diccionario
                #tambien se elimina esos bits de la varible reemplazando con vacio
                if Bdic == beta[0:len(Bdic)]:    
            
                    LiDic.append(xval[i][k])
                    beta = beta.replace(Bdic,'',1)
        
        #reacondicionar
        Lrp = np.array(LiDic)
        Lrp = np.reshape(Lrp,(Mx,Nx))
        
        #
        arreBanda.append(Lrp)
        arrefe.append(fex)
        arrerp.append(rpx)
    
    return arreBanda,arrefe,arrerp


def Cuanti(bcompri,fe,RP):
    
    [F] = np.shape(bcompri)
    
    BandaNivelado = []

    val_ecu = (fe[0])/(pow(2,(RP[0]-1))-1)
    BandaNivelado.append(bcompri[0]*val_ecu)

    for i in range(1,F):
        val_ecu = (fe[i])/(pow(2,(RP[i]-1))-1)
        BandaNivelado.append((bcompri[i]*val_ecu))

    return BandaNivelado

def Reconstruc(BandaNivelado,wavelet_nor):

    Y3  = pywt.idwt2((BandaNivelado[0],(BandaNivelado[1],BandaNivelado[2],None)),wavelet_nor,'periodization')
    Y2  = pywt.idwt2((Y3,(None,None,None)),wavelet_nor,'periodization')
    Y1  = pywt.idwt2((Y2,(None,None,None)),wavelet_nor,'periodization')

    CB4 = pywt.idwt2((BandaNivelado[3],(None,None,None)),wavelet_nor,'periodization')
    CB3 = pywt.idwt2((CB4,(None,None,None)),wavelet_nor,'periodization')
    CB2 = pywt.idwt2((CB3,(None,None,None)),wavelet_nor,'periodization')
    CB1 = pywt.idwt2((CB2,(None,None,None)),wavelet_nor,'periodization')

    CR4 = pywt.idwt2((BandaNivelado[4],(None,None,None)),wavelet_nor,'periodization')
    CR3 = pywt.idwt2((CR4,(None,None,None)),wavelet_nor,'periodization')
    CR2 = pywt.idwt2((CR3,(None,None,None)),wavelet_nor,'periodization')
    CR1 = pywt.idwt2((CR2,(None,None,None)),wavelet_nor,'periodization')



    return Y1,CB1,CR1

def UpSampling(Y1,CB1,CR1):
    #formato 420
    #upsampling orden 0
    [tf,tc] = Y1.shape
    CBI = np.zeros((tf,tc))
    CRI = np.zeros((tf,tc))

    for f in range(0,ceil(tf/2)):
        for c in range(0,ceil(tc/2)):
            CBI[2*f,2*c] = CB1[f,c]
    
    CBI[:,1::2] = CBI[:,0::2]
    CBI[1::2,:]  = CBI[0::2,:]

    for f in range(0,ceil(tf/2)):
        for c in range(0,ceil(tc/2)):
            CRI[2*f,2*c] = CR1[f,c]
    
    CRI[:,1::2] = CRI[:,0::2]
    CRI[1::2,:]  = CRI[0::2,:]

    return CBI,CRI
    
def convertYtoRGB(Y1,CB1,CR1,Z):

    #R1 = 0.25*Y1 + 0.50*CB1 + 0.25*CR1
    #G1 = 0.5*Y1 - 0.5*CR1
    #B1 = -0.25*Y1 + 0.50*CB1 - 0.25*CR1

    R1 = 1*Y1 + 1*CB1 - 1*CR1
    G1 = 1*Y1 + 1*CR1
    B1 = 1*Y1 - 1*CB1 - 1*CR1


    R1 = np.round(R1)
    G1 = np.round(G1)
    B1 = np.round(B1)

    R1=UEntero8(R1)
    G1=UEntero8(G1)
    B1=UEntero8(B1)
    
    Z[:,:,0]=R1
    Z[:,:,1]=G1
    Z[:,:,2]=B1
    Z=np.uint8(Z)

    return Z

#calculos para el codificador
def ObterneTiempo(posi):
    duty=20
    paquetes=37

    if posi==0:
        SF=7
        ToA=98.624
        TTX=ToA*(10**(-3))*duty*paquetes
        TTX=round((TTX/60),2)
    elif posi==1:
        SF=8
        ToA=174.208
        TTX=ToA*(10**(-3))*duty*paquetes
        TTX=round((TTX/60),2)
    elif posi==2:
        SF=9
        ToA=312.576
        TTX=ToA*(10**(-3))*duty*paquetes
        TTX=round((TTX/60),2)
    elif posi==3:
        SF=10
        ToA=563.721
        TTX=ToA*(10**(-3))*duty*paquetes
        TTX=round((TTX/60),2)
    elif posi==4:
        SF=11
        ToA=1025.024
        TTX=ToA*(10**(-3))*duty*paquetes
        TTX=round((TTX/60),2)
    elif posi==5:
        SF=12
        ToA=1927.168
        TTX=ToA*(10**(-3))*duty*paquetes
        TTX=round((TTX/60),2)
    
    return SF,TTX

def ConvertY1to1(Y1,CB1,CR1,F1,F2,F3):
    R1=1*Y1 + 1*CB1 - 1*CR1
    G1=1*Y1 + 1*CR1
    B1=1*Y1 - 1*CB1 - 1*CR1

    R1 = np.round(R1)
    G1 = np.round(G1)
    B1 = np.round(B1)

    R1=UEntero8(R1)
    G1=UEntero8(G1)
    B1=UEntero8(B1)
    
    F1[:,:,0]=R1

    F2[:,:,0]=R1
    F2[:,:,1]=G1

    F3[:,:,0]=R1
    F3[:,:,1]=G1
    F3[:,:,2]=B1

    F1 = np.uint8(F1)
    F2 = np.uint8(F2)
    F3 = np.uint8(F3)

    return F1,F2,F3