import tkinter as tk
from PIL import Image, ImageTk
from ProcesosFHNV2 import *
import cv2
from tkinter import filedialog,ttk
from time import time
import time

root = tk.Tk()
root.title("Interfaz del Decodificador")
root.geometry("520x700")

global barra

def ejecutarTotal(): 
    global new_pic
    val = ComboTasa['values'].index(ComboTasa.get())

    if val==1:
        barra.step(100)
        Irgb = cv2.imread('Imagenes/img1.jpg')
        I1 = cv2.cvtColor(Irgb,cv2.COLOR_BGR2RGB)
        [Iy,ICo,ICg]    = RGBtoYCOCG(I1)
        [ICo1,ICg1]     = SubSampling(ICo,ICg)
        [Ya3,YH3,YV3,CoA4,CgA4] = Descom(Iy,ICo1,ICg1,Nwav35bio)
        [bcompri,fe,RP] = Recuan(Ya3,YH3,YV3,CoA4,CgA4,6,5,5)
        #print(fe)
        [frec,xval]     = Peso(bcompri) #frecuencia de cada pixelxs
        [bitsOut, Dic]= EmpaqueH(frec,xval,bcompri,fe,RP)
        
        [arreBanda,arrefe,arrerp] = DesempaqueH(5,xval,Dic)#desempaquetado con Huffman
        BandaNivelado   = Cuanti(arreBanda,fe,arrerp)
        [Y1,CB1,CR1]    = Reconstruc(BandaNivelado,Nwav35bio)
        [CBI,CRI]       = UpSampling(Y1,CB1,CR1)
    
        [M,N]=np.shape(CBI)
        G=np.zeros((M,N,3))

        Z = convertYtoRGB(Y1,CBI,CRI,G)

    
        resized = cv2.resize(Z,(93,564),interpolation=cv2.INTER_CUBIC)
        new_pic = ImageTk.PhotoImage(image=Image.fromarray(resized))
        Limage  = tk.Label(root,image=new_pic).place(x=400,y=60)

        barra.step(99.9999)
    elif val==0:
        barra.step(100)
        Irgb = cv2.imread('Imagenes/img1.jpg')
        I1 = cv2.cvtColor(Irgb,cv2.COLOR_BGR2RGB)
        [Iy,ICo,ICg]    = RGBtoYCOCG(I1)
        [ICo1,ICg1]     = SubSampling(ICo,ICg)
        [Ya3,YH3,YV3,CoA4,CgA4] = Descom(Iy,ICo1,ICg1,Nwav35bio)
        [bcompri,fe,RP] = Recuan(Ya3,YH3,YV3,CoA4,CgA4,6,5,5)
        #print(fe)
        [frec,xval]     = Peso(bcompri) #frecuencia de cada pixelxs
        [bitsOut, Dic]= EmpaqueH(frec,xval,bcompri,fe,RP)
        [arreBanda,arrefe,arrerp] = DesempaqueH(5,xval,Dic)#desempaquetado con Huffman
        BandaNivelado   = Cuanti(arreBanda,fe,arrerp)
        [Y1,CB1,CR1]    = Reconstruc(BandaNivelado,Nwav35bio)
        [CBI,CRI]       = UpSampling(Y1,CB1,CR1)

        [M,N]=np.shape(CBI)
        F1=np.zeros((M,N,3))
        F2=np.zeros((M,N,3))
        F3=np.zeros((M,N,3))

        [Z1,Z2,Z3] = ConvertY1to1(Y1,CBI,CRI,F1,F2,F3)

        Z = [Z1,Z2,Z3]

        def ha1():
            global new_pic
            
            resized = cv2.resize(Z[0],(93,564),interpolation=cv2.INTER_CUBIC)
            new_pic = ImageTk.PhotoImage(image=Image.fromarray(resized))
            Limage  = tk.Label(root,image=new_pic).place(x=400,y=60)
            barra.step(30)
            root.after(1000,ha2)
        def ha2():
            global new_pic
            
            resized = cv2.resize(Z[1],(93,564),interpolation=cv2.INTER_CUBIC)
            new_pic = ImageTk.PhotoImage(image=Image.fromarray(resized))
            Limage  = tk.Label(root,image=new_pic).place(x=400,y=60)
            barra.step(60)
            root.after(1000,ha3)
        def ha3():
            global new_pic
            resized = cv2.resize(Z[2],(93,564),interpolation=cv2.INTER_CUBIC)
            new_pic = ImageTk.PhotoImage(image=Image.fromarray(resized))
            Limage  = tk.Label(root,image=new_pic).place(x=400,y=60)
            barra.step(9.9)

        
        
        root.after(1000,ha1)
            
            
            

        



        


    

    Lnameo = tk.Label(root,text="Imagen Reconstruida",font='Helvetica 15 bold').place(x=360,y=40)


Ltmodo = tk.Label(root,text="Modo de Reconstruccion:",font='Helvetica 13 bold').place(x=10,y=120)
Ltestado = tk.Label(root,text="Estado de Recepcion:",font='Helvetica 13 bold').place(x=10,y=160)


ComboTasa = ttk.Combobox(root,width=10)
ComboTasa['values']=("Rx y Mostrar","Rx Todo y Mostrar")
ComboTasa.place(x=200,y=120)
ComboTasa.current(0)

barra = ttk.Progressbar(root,orient=tk.HORIZONTAL,length=100,mode='determinate')
barra.place(x=100,y=200,width=200)


Bexe   = tk.Button(root,text="Ejecutar",fg="red",command=ejecutarTotal).place(x=60,y=80)
#Bbandas = tk.Button(root,text="Obtener Bandas",fg="red",command=CalTransmit).place(x=60,y=240)

root.mainloop() 