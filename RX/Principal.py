
from Procesos import *
import cv2
import matplotlib.pyplot as plt
import tkinter

Irgb = cv2.imread('blanco1-ultimo.png')
I1 = cv2.cvtColor(Irgb,cv2.COLOR_BGR2RGB)

[IY,ICB,ICR]    = SubSampling(I1)
[YA2,CBA2,CRA2] = Descom(IY,ICB,ICR,Nwav35bio)
[bcompri,fe,RP] = Recuan(YA2,CBA2,CRA2,6,6,6)
[bitsIn,bitsOut]= Empaque(bcompri,fe,RP,Irgb)

# [arreBanda,arrefe,arrerp] = Desempaque(3)
# BandaNivelado   = Cuanti(arreBanda,arrefe,arrerp)
# [Y1,CB1,CR1]    = Reconstruc(BandaNivelado,Nwav35bio)
# [CBI,CRI]       = UpSampling(Y1,CB1,CR1)

# [M,N,P]=np.shape(Irgb)
# G=np.zeros((M,N,P))

# Z=convertYtoRGB(Y1,CBI,CRI,G)


# plt.figure(1)
# plt.imshow(Z)

# plt.figure(2)
# plt.imshow(I1)
# plt.show()

# def AgrupaBits(frec,xval):
#     #armar el diccionario
#     cbook=[]
#     for k in range(len(frec)):
#         cbook.append((xval[k],frec[k]))

#     diccionario = huffman.codebook(cbook)

#     #armar LOS BITS
#     Bagrup = ''
#     for i in range(len(xval)):
#         Bagrup = Bagrup + diccionario[xval[i]]*frec[i]

#     return Bagrup

# def AgrupaBits(frec,xval,subM):
#     #armar el diccionario
#     cbook=[]
#     for k in range(len(frec)):
#         cbook.append((xval[k],frec[k]))

#     diccionario = huffman.codebook(cbook)
#     print(diccionario)
#     #armar LOS BITS
#     [f,c]=np.shape(subM)

#     Bagrup = ''
#     for i in range(f):
#         for j in range(c):
#             #print(int(subM[i,j]))
#             Bagrup = Bagrup + diccionario[int(subM[i,j])]
            
#     return Bagrup