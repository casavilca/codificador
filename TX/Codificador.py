import tkinter as tk
from PIL import Image, ImageTk
from Procesos import *
import cv2
from tkinter import filedialog,ttk
from time import time

root = tk.Tk()
root.title("Interfaz del Codificador")
root.geometry("520x700")

def openclickfile():
    global new_pic

    filename = filedialog.askopenfile(initialdir="/Users/kv/Desktop/Algoritmo-Proyecto1-python/Imagenes/",title='Seleciona un archivo xd:',filetypes=(("png files","*.png"),("all files","*.*")))

    tiempo_inicio=time()
    I1 = Image.open(filename.name)

    [Iy,ICo,ICg]    = RGBtoYCOCG(I1)
    [ICo1,ICg1]     = SubSampling(ICo,ICg)
    [Ya3,YH3,YV3,CoA4,CgA4] = Descom(Iy,ICo1,ICg1,Nwav35bio)
    [bcompri,fe,RP] = Recuan(Ya3,YH3,YV3,CoA4,CgA4,6,5,5)
    [frec,xval]     = Peso(bcompri) #frecuencia de cada pixelxs
    [bitsOut, Dic]= EmpaqueH(frec,xval,bcompri,fe,RP)
    
    bitsIn = 1504*248*8*3

    tiempo_final=time()
    #mostrar imagen seleccionada
    I1 = np.array(I1)
    resized = cv2.resize(I1,(93,564),interpolation=cv2.INTER_CUBIC)
    new_pic = ImageTk.PhotoImage(image=Image.fromarray(resized))
    #Limage  = tk.Label(root,image=new_pic).pack(padx=20,side="right")
    Limage  = tk.Label(root,image=new_pic).place(x=400,y=60)

    
    Lnameo = tk.Label(root,text="Imagen Seleccionada",font='Helvetica 15 bold').place(x=360,y=40)

    Lnin = tk.Label(root,text=str(bitsIn)+" bits").place(x=200,y=120)
    Lnou = tk.Label(root,text=str(bitsOut)+" bits").place(x=200,y=160)
    Lnfc = tk.Label(root,text=str(round((bitsIn/bitsOut),3))).place(x=200,y=200)

    Lnej = tk.Label(root,text=str(round((tiempo_final-tiempo_inicio),3))+" segundos").place(x=200,y=400)

def CalTransmit():
    
    [SF,TTX] = ObterneTiempo(ComboTasa['values'].index(ComboTasa.get()))

    

    Lnsf = tk.Label(root,text=str(SF),height=2, width=9).place(x=200,y=320)
    Lntx = tk.Label(root,text=str(TTX)+" minutos",height=2, width=9).place(x=200,y=360)


Ltin = tk.Label(root,text="Tamaño imagen Ingresado:",font='Helvetica 13 bold').place(x=10,y=120)
Ltou = tk.Label(root,text="Tamaño imagen Salida:",font='Helvetica 13 bold').place(x=10,y=160)
Ltfc = tk.Label(root,text="Factor de Compresión:",font='Helvetica 13 bold').place(x=10,y=200)

Ltta = tk.Label(root,text="Tasa de bit:",font='Helvetica 13 bold').place(x=10,y=280)
Ltsf = tk.Label(root,text="Spreading Factor:",font='Helvetica 13 bold').place(x=10,y=320)
Lttx = tk.Label(root,text="Tiempo de Tx:",font='Helvetica 13 bold').place(x=10,y=365)
Ltej = tk.Label(root,text="Tiempo de Ejecución:",font='Helvetica 13 bold').place(x=10,y=400)

#Tin = tk.Entry(root,width=10)
#Tin.grid(column=0,row=1)
#Tin.place(x=210,y=120)

ComboTasa = ttk.Combobox(root,width=10)
ComboTasa['values']=("21.87 Kbps","12.50 Kbps","7.03 Kbps","3.90 Kbps","2.15 Kbps","1.17 Kbps")
ComboTasa.place(x=200,y=280)
ComboTasa.current(0)

Bopen   = tk.Button(root,text="Seleccionar Imagen",fg="blue",command=openclickfile).place(x=60,y=80)
Bbandas = tk.Button(root,text="Obtener Bandas",fg="red",command=CalTransmit).place(x=60,y=240)

root.mainloop() 