import tkinter as tk
import tkinter.ttk as ttk
from tkinter import filedialog
from PIL import Image, ImageTk
from ProcesosFHNV2 import *
import cv2

class App:
    def __init__(self, master):
        self.master = master
        self.master.title("Interfaz del Codificador")
        self.master.geometry("+50+50")
        self.master.configure(bg="#398AEC")
        
        frm = tk.Frame(self.master,bg="#398AEC")
        frm.pack(padx=10, pady=10)
                
        self.frm1 = tk.Frame(frm,bg="#398AEC")
        self.frm2 = tk.Frame(frm,bg="#398AEC")
        
        self.frm1.pack(side=tk.LEFT, anchor=tk.N)
        self.frm2.pack(side=tk.LEFT)
        
        self.frm1_1 = tk.LabelFrame(self.frm1, text="Archivo", width=600,bg="#398AEC")
        self.frm1_2 = tk.LabelFrame(self.frm1, text="Bandas",bg="#398AEC")
        
        self.frm1_1.pack(padx=5, pady=5, fill=tk.X)
        self.frm1_2.pack(padx=5, pady=5)
        
        self.frm2_1 = tk.LabelFrame(self.frm2, text="Imagen",bg="#398AEC")
        self.frm2_1.pack(padx=5, pady=5)
 
        # ------------------------- frm 1_1 ------------------------------
        self.lblImagenIn = tk.Label(self.frm1_1, text="Tamaño Imagen In: ",bg="#398AEC")
        self.lblImagenOut = tk.Label(self.frm1_1, text="Tamaño Imagen Out: ",bg="#398AEC")
        self.lblFactorComp = tk.Label(self.frm1_1, text="Factor Compresion: ",bg="#398AEC")

        #self.lblImagenInData = tk.Label(self.frm1_1, font='Cambria 12 bold', text="{:,.0f} bits".format(8951808))
        #self.lblImagenOutData = tk.Label(self.frm1_1, text=f"{'':45}")
        #self.lblFactorCompData = tk.Label(self.frm1_1, text=f"{'':45}")

        self.btnSeleccionarImg = tk.Button(self.frm1_1, text="Seleccionar Imagen",command=self.openclickfile, width=16,bg="#398AEC")
        
        self.lblImagenIn.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)
        self.lblImagenOut.grid(row=1, column=0, padx=5, pady=5, sticky=tk.W)
        self.lblFactorComp.grid(row=2, column=0, padx=5, pady=5, sticky=tk.W)
        #self.lblImagenInData.grid(row=0, column=1, padx=5, pady=5, sticky=tk.W)
        #self.lblImagenOutData.grid(row=1, column=1, padx=5, pady=5, sticky=tk.W)
        #self.lblFactorCompData.grid(row=2, column=1, padx=5, pady=5, sticky=tk.W)
        self.btnSeleccionarImg.grid(row=3, column=2, padx=5, pady=5, sticky=tk.E)

        # ------------------------- frm 1_2 ------------------------------
        self.lblTasaBits = tk.Label(self.frm1_2, text="Tasa de bits:",bg="#398AEC")
        self.lblSpeadingFactor = tk.Label(self.frm1_2, text="Spreading Factor: ",bg="#398AEC")
        self.lblTiempoTx = tk.Label(self.frm1_2, text="Tiempo Tx: ",bg="#398AEC")
        self.lblTiempoEjec = tk.Label(self.frm1_2, text="Tiempo de ejecucion: ",bg="#398AEC")


        self.cboTasas = ttk.Combobox(self.frm1_2)
        self.cboTasas['values']=("21.87 Kbps","12.50 Kbps","7.03 Kbps","3.90 Kbps","2.15 Kbps","1.17 Kbps")
        self.cboTasas.current(0)

        self.btnObtenerTasas = tk.Button(self.frm1_2, text="Obtener bandas", command=self.CalTransmit,width=16,bg="#398AEC")
        #self.lblSpeadingFactorData = tk.Label(self.frm1_2, text=f"{'':45}")
        #self.lblTiempoTxData = tk.Label(self.frm1_2, text=f"{'':45}")
        #self.lblTiempoEjecData = tk.Label(self.frm1_2, text=f"{'*':45}")
        
        self.lblTasaBits.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)
        self.lblSpeadingFactor.grid(row=1, column=0, padx=5, pady=5, sticky=tk.W)
        self.lblTiempoTx.grid(row=2, column=0, padx=5, pady=5, sticky=tk.W)
        self.lblTiempoEjec.grid(row=3, column=0, padx=5, pady=5, sticky=tk.W)
        self.cboTasas.grid(row=0, column=1, padx=5, pady=5)
        self.btnObtenerTasas.grid(row=0, column=2, padx=5, pady=5, sticky=tk.E)
        #self.lblSpeadingFactorData.grid(row=1, columnspan=2, column=1, padx=5, pady=5)
        #self.lblTiempoTxData.grid(row=2, column=1, columnspan=2, padx=5, pady=5)
        #self.lblTiempoEjecData.grid(row=3, column=1, columnspan=2, padx=5, pady=5)
        
        # ------------------------- frm 2_1 ------------------------------
        self.canvasImg = tk.Canvas(self.frm2_1, height=600, width=120,bg="#7DB0EE")
        self.canvasImg.pack(expand=True, fill=tk.BOTH)

    def openclickfile(self):
        global new_pic

        self.filename = filedialog.askopenfile(initialdir="/Users/kv/Documents/Cursos2020-2/Proyecto 2/PARCIAL/Imagenes/",title='Seleciona una imagen:',filetypes=(("jpg files","*.jpg"),("all files","*.*")))
        
        #self.tiempo_inicio=time()
        I1 = Image.open(self.filename.name)
        # [Iy,ICo,ICg]    = RGBtoYCOCG(I1)
        # [ICo1,ICg1]     = SubSampling(ICo,ICg)
        # [Ya3,YH3,YV3,CoA4,CgA4] = Descom(Iy,ICo1,ICg1,Nwav35bio)
        # [bcompri,fe,RP] = Recuan(Ya3,YH3,YV3,CoA4,CgA4,6,5,5)
        # [frec,xval]     = Peso(bcompri) #frecuencia de cada pixelxs
        # [bitsOut, Dic]= EmpaqueH(frec,xval,bcompri,fe,RP)

        #self.tiempo_final=time()

        #mostrar imagen seleccionada
        I1 = np.array(I1)
        resized = cv2.resize(I1,(93,564),interpolation=cv2.INTER_CUBIC)
        new_pic = ImageTk.PhotoImage(image=Image.fromarray(resized))

        my_image=self.canvasImg.create_image(60, 300, image=new_pic)


        self.lblImagenInData = tk.Label(self.frm1_1, font='Cambria 12 bold', text="8951808",bg="#398AEC")
        self.lblImagenInData.grid(row=0, column=1, padx=5, pady=5, sticky=tk.W)
        #self.lblTiempoEjecData = tk.Label(self.frm1_2,font='Cambria 12 bold', text=str(round((tiempo_final-tiempo_inicio),3))+" segundos")
        #self.lblTiempoEjecData.grid(row=3, column=1, columnspan=1, padx=5, pady=5)

    def CalTransmit(self):
    
        [SF,TTX] = ObterneTiempo(self.cboTasas['values'].index(self.cboTasas.get()))

        self.tiempo_inicio=time()
        I1 = Image.open(self.filename.name)
        [Iy,ICo,ICg]    = RGBtoYCOCG(I1)
        [ICo1,ICg1]     = SubSampling(ICo,ICg)
        [Ya3,YH3,YV3,CoA4,CgA4] = Descom(Iy,ICo1,ICg1,Nwav35bio)
        [bcompri,fe,RP] = Recuan(Ya3,YH3,YV3,CoA4,CgA4,6,5,5)
        [frec,xval]     = Peso(bcompri) #frecuencia de cada pixelxs
        [bitsOut, Dic]= EmpaqueH(frec,xval,bcompri,fe,RP)
        bitsIn = 1504*248*8*3
        self.tiempo_final=time()

        self.lblImagenOutData = tk.Label(self.frm1_1,font='Cambria 12 bold', text="{:.0f}".format(bitsOut),bg="#398AEC")
        self.lblFactorCompData = tk.Label(self.frm1_1,font='Cambria 12 bold', text=str(round((bitsIn/bitsOut),3)),bg="#398AEC")
        self.lblImagenOutData.grid(row=1, column=1, padx=5, pady=5, sticky=tk.W)
        self.lblFactorCompData.grid(row=2, column=1, padx=5, pady=5, sticky=tk.W)

        self.lblSpeadingFactorData = tk.Label(self.frm1_2,font='Cambria 12 bold', text=str(SF),bg="#398AEC")
        self.lblTiempoTxData = tk.Label(self.frm1_2,font='Cambria 12 bold',text=str(TTX)+" minutos",bg="#398AEC")

        self.lblSpeadingFactorData.grid(row=1, columnspan=1, column=1, padx=5, pady=5)
        self.lblTiempoTxData.grid(row=2, column=1, columnspan=1, padx=5, pady=5)

        self.lblTiempoEjecData = tk.Label(self.frm1_2,font='Cambria 12 bold', text=str(round((self.tiempo_final-self.tiempo_inicio),3))+" segundos",bg="#398AEC")
        self.lblTiempoEjecData.grid(row=3, column=1, columnspan=1, padx=5, pady=5)
       


        
        
        
root = tk.Tk()
app = App(root)
root.mainloop()