import serial
import time
import numpy as np
import math
import cv2
import matplotlib.pyplot as plt
import numpy as np
import PIL
import os

ser=serial.Serial('COM6') #ENTRAR SOMENTE 
ser.baudrate = 115200 #VELOCIDAD DEL PUERTO
ser.timeout=0.5 #Segundo
#ser.close()
time.sleep(1)
#ser.open()

print("PUERTO:", ser.name)

#VARIABLES
is_header=False
is_data_image=False
is_data_info=False
huffman_active=False
is_last_package=False
counter=b''
payload_size=144
is_last_package=False




#RELLENO
data_fill=np.zeros((1,144),'uint')
data_fill=data_fill.tolist()
data_fill=data_fill[0]

contador=0;
contador_paquete=0
contador_bytes=0
primero_key=0
segundo_key=0
contador_key=0



archivo_aux=[]
archivo1=[]
archivo2=[]
arhcivo3=[]
archivo4=[]
arhcivo5=[]
file=1


def create_file(data):
	ctn=0
	for i in range(0,len(data),3):
		print("Contador",i)
		ctn += 1

		tam_banda_real=data[i]
		tam_banda_real=tam_banda_real[0]
		
		tam_banda_recibida=len(data[i+2])

		print(tam_banda_real,tam_banda_recibida)
		print(type(tam_banda_real),type(tam_banda_recibida))
		
		f = open('headerData'+str(ctn)+'.pjt', 'wb')
		f.write(bytearray(data[i+1]))
		f.close()

		if(tam_banda_real==tam_banda_recibida):			
			f2 = open('archivoH'+str(ctn)+'.pjt','wb')
			f2.write(bytearray(data[i+2]))
			f2.close()

		elif(tam_banda_real>tam_banda_recibida):
			data_to_add=np.zeros((1,(tam_banda_real-tam_banda_recibida)),'uint')
			data_to_add=data_to_add.tolist()
			data_to_add=data_to_add[0]
			

			data_aux=data[i+2]
			data_aux.extend(data_to_add)

			f2 = open('archivoH'+str(ctn)+'.pjt','wb')
			f2.write(bytearray(data_aux))
			f2.close()

		elif(tam_banda_real<tam_banda_recibida):
			#data_to_add=np.zeros((1,(tam_banda_real-tam_banda_recibida)),'uint')
			#data_to_add=data_to_add.tolist()
			#data_to_add=data_to_add[0]
			print("Se quitaron bytes bytes")
			data_aux=data[i+2]
			f2 = open('archivoH'+str(ctn)+'.pjt','wb')
			f2.write(bytearray(data_aux[0:tam_banda_real]))
			f2.close()

def receive_data(data_received,band_counter,band_received,is_huffman,received_already_header):
	#VARIABLES
	print("INICIANDO RECEPCION DE DATOS")
	max_package=0
	package_counter=0
	header_counter=0
	isAlreadyBand=False

	while True:
		#time.sleep(1.5)		
		#ser.flush()
		data_rec=ser.read(150)

		if(len(data_rec)!=0):
			heder_data=data_rec[0:5]
			heder_data=heder_data.split(b';')

			payload=data_rec[6:]
			#print("PAYLOAD TAMAÑO RECIBIDO: ",len(payload))

			if(heder_data[0]==b'n' and heder_data[1]==b's' and received_already_header==True):

				package_counter += 1
				
				#print(len(data_rec),int.from_bytes(heder_data[2],'big'),data_rec[0:10])
				print(package_counter,int.from_bytes(heder_data[2],'big'))

				if(package_counter==int.from_bytes(heder_data[2],'big')):
					band_received.extend(list(payload))

				else:
					#print("is_huffman",is_huffman)
					if(is_huffman):
						#is_waiting_reSend=True
						header="reTX".encode()
						data=header+b';'+bytes([band_counter])
						print("DATA TO SEND:", data,band_counter)
						ser.write(data)
						print("Re-starting reception Huffman")

						return False,data_received[0:-2],band_counter

					res=(int.from_bytes(heder_data[2],'big'))-package_counter
					print("RESTA DE PAQUETES: ",res)
					band_received.extend(data_fill*res)
					band_received.extend(list(payload))
					package_counter += res
					ctn_loss_package += res

				header_counter=0
				isAlreadyBand=True

			elif(heder_data[0]==b's' and heder_data[1]==b's'):
				#is_waiting_reSend=False
				header_counter += 1

				received_already_header=True

				#Gettin band size
				Size_band=int.from_bytes(payload[11:13],'big')	
				Size_band=int(Size_band/8)
				
				#
				if(header_counter==1):
					if(isAlreadyBand):
						if(package_counter==max_package):
							print("COMPARA PAQUETE: ",package_counter,max_package)
							data_received.append(band_received)
							band_counter += 1
						else:
							return False, data_received[0:-2]

					data_received.append([Size_band])
					data_received.append(list(payload))
					print("TAMAÑO DE LISTA DATA: ",len(data_received))
					#band_counter += 1

					print("TAMAÑO DEL RECIBIDO: ",len(band_received))

				# Getting data type(Huffman/no huffman)
				data_position_table=int.from_bytes(payload[13:14],'big')
				#print("POSICION :", data_position_table)

				#Getting max count package
				max_package=math.ceil(Size_band/payload_size)


				band_position=int.from_bytes(heder_data[2],'big')

				#if(band_position==1 and header_counter==1):
				#	print("Primer Archivo recepcionado")
				#	data_received=[]

				if(data_position_table!=66):
					is_huffman=True
				else:
					is_huffman=False

				print("TAMAÑO DE LA BANDA A RECIBIR : ",Size_band)
		
				if(payload==b'fn'):
					#return True,data_received[1:(len(data_received)-2)] #RECORTE DE LA INFORMACION INPORTANTE
					return True, data_received[0:-2],band_counter
				#REINICIANDO VARIABLES
				band_received=[]
				package_counter=0

	#print("CANTIDAD DE PAQUETES PERDIDOS: ",ctn_loss_package)
	#return True,data_received #RECORTE DE LA INFORMACION INPORTANTE
		



def run():
	while True:
		ser.flushInput()
		data_received=[]
		band_received=[]
		band_counter=1
		
		#header_counter=0
		#ctn_loss_package=0
		#received_already_header=False
		is_huffman=False

		while True:
			received_already_header=False
			received_ok,data_bandas,band_counter=receive_data(data_received,band_counter,band_received,is_huffman,received_already_header)
			data_received=data_bandas
			print("NUMERO DE BANDA: ",band_counter)
			if(received_ok):
				create_file(data_received)
				for dt in data_received:
					if(len(dt)==1):
						print(dt[0])
					else:
						print(len(dt))
				print("==========================Sleepin")
				time.sleep(2)
				print("================I wake up==================")
				break
			


run()


# def run():
# 	send_ok=True
# 	while True:

# 		if(send_ok):
# 			t_start=time.time()

# 		send_ok=send_data()

# 		if(send_ok):
# 			t_final=time.time()
# 			print("Tiempo de TX :",int(t_final-t_start))
# 			print("==========================Sleeping============================")
# 			break
# 			#time.sleep(30)
# 			#print("====I'm ready to work========")

# run()
# try:
# 	os.system('python DecoderNH.py')
# except Exception as e:
# 	print("No se puede reconstruir la imagen")
# 	print(e)

