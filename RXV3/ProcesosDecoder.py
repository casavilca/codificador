import numpy as np
import pywt
from math import ceil
import Tablas

class Bio39FilterBank(object):
    @property
    def filter_bank(self):

        cdl1 = -0.000239631196178
        cdl2 =  0.000718893588534
        cdl3 =  0.001783921127103
        cdl4 = -0.007268812950735
        cdl5 = -0.004975200073030
        cdl6 =  0.034948117944193
        cdl7 =  0.004336183549889
        cdl8 = -0.112877704409214
        cdl9 =  0.000722697258315
        cdl10=  0.332128837902809
        cdl11=  0.332128837902809
        cdl12=  0.000722697258315
        cdl13= -0.112877704409214
        cdl14=  0.004336183549889
        cdl15=  0.034948117944193
        cdl16= -0.004975200073030
        cdl17= -0.007268812950735
        cdl18=  0.001783921127103
        cdl19=  0.000718893588534
        cdl20= -0.000239631196178


        cdh1  =  0
        cdh2  =  0
        cdh3  =  0
        cdh4  =  0
        cdh5  =  0
        cdh6  =  0
        cdh7  =  0
        cdh8  =  0
        cdh9  = -0.125000000000000
        cdh10 =  0.375000000000000
        cdh11 = -0.375000000000000
        cdh12 =  0.125000000000000
        cdh13 =  0
        cdh14 =  0
        cdh15 =  0
        cdh16 =  0
        cdh17 =  0
        cdh18 =  0
        cdh19 =  0
        cdh20 =  0

        crl1  = 0
        crl2  = 0
        crl3  = 0
        crl4  = 0
        crl5  = 0
        crl6  = 0
        crl7  = 0
        crl8  = 0
        crl9  = 0.501449584960938
        crl10 = 1.504348754882813
        crl11 = 1.504348754882813
        crl12 = 0.501449584960938
        crl13 = 0
        crl14 = 0
        crl15 = 0
        crl16 = 0
        crl17 = 0
        crl18 = 0
        crl19 = 0
        crl20 = 0

        crh1  = -0.000961303710938
        crh2  = -0.002883911132813
        crh3  =  0.007156372070313
        crh4  =  0.029159545898438
        crh5  = -0.019958496093750
        crh6  = -0.140197753906250
        crh7  =  0.017395019531250
        crh8  =  0.452819824218750
        crh9  =  0.002899169921875
        crh10 = -1.332366943359375
        crh11 =  1.332366943359375
        crh12 = -0.002899169921875
        crh13 = -0.452819824218750
        crh14 = -0.017395019531250
        crh15 =  0.140197753906250
        crh16 =  0.019958496093750
        crh17 = -0.029159545898438
        crh18 = -0.007156372070313
        crh19 =  0.002883911132813
        crh20 =  0.000961303710938


        dec_lo = [cdl1,cdl2,cdl3,cdl4,cdl5,cdl6,cdl7,cdl8,cdl9,cdl10,cdl11,cdl12,cdl13,cdl14,cdl15,cdl16,cdl17,cdl18,cdl19,cdl20]
        dec_hi = [cdh1,cdh2,cdh3,cdh4,cdh5,cdh6,cdh7,cdh8,cdh9,cdh10,cdh11,cdh12,cdh13,cdh14,cdh15,cdh16,cdh17,cdh18,cdh19,cdh20]
        rec_lo = [crl1,crl2,crl3,crl4,crl5,crl6,crl7,crl8,crl9,crl10,crl11,crl12,crl13,crl14,crl15,crl16,crl17,crl18,crl19,crl20]
        rec_hi = [crh1,crh2,crh3,crh4,crh5,crh6,crh7,crh8,crh9,crh10,crh11,crh12,crh13,crh14,crh15,crh16,crh17,crh18,crh19,crh20]
        
        return [dec_lo, dec_hi, rec_lo, rec_hi]
filter_bank = Bio39FilterBank()
Nwav39bio = pywt.Wavelet(name="myWavelet39", filter_bank=filter_bank)

class Bio35FilterBank(object):
    @property
    def filter_bank(self):

        cdl1 = -0.0048828125
        cdl2 =  0.0146484375
        cdl3 =  0.0185546875
        cdl4 = -0.0947265625
        cdl5 = -0.0253906250
        cdl6 =  0.3417968750
        cdl7 =  0.3417968750
        cdl8 = -0.0253906250
        cdl9 = -0.0947265625
        cdl10=  0.0185546875
        cdl11=  0.0146484375
        cdl12= -0.0048828125

        cdh1 =  0
        cdh2 =  0
        cdh3 =  0
        cdh4 =  0
        cdh5 = -0.1250
        cdh6 =  0.3750
        cdh7 = -0.3750
        cdh8 =  0.1250
        cdh9 =  0
        cdh10 = 0
        cdh11 = 0
        cdh12 = 0

        crl1 = 0
        crl2 = 0
        crl3 = 0
        crl4 = 0
        crl5 = 0.5
        crl6 = 1.5
        crl7 = 1.5
        crl8 = 0.5
        crl9 = 0
        crl10= 0
        crl11= 0
        crl12= 0

        crh1 =  -0.01953125
        crh2 =  -0.05859375
        crh3 =   0.07421875
        crh4 =   0.37890625
        crh5 =  -0.10156250
        crh6 =  -1.36718750
        crh7 =   1.36718750
        crh8 =   0.10156250
        crh9 =  -0.37890625
        crh10 = -0.07421875
        crh11 =  0.05859375
        crh12 =  0.01953125

        dec_lo = [cdl1,cdl2,cdl3,cdl4,cdl5,cdl6,cdl7,cdl8,cdl9,cdl10,cdl11,cdl12]
        dec_hi = [cdh1,cdh2,cdh3,cdh4,cdh5,cdh6,cdh7,cdh8,cdh9,cdh10,cdh11,cdh12]
        rec_lo = [crl1,crl2,crl3,crl4,crl5,crl6,crl7,crl8,crl9,crl10,crl11,crl12]
        rec_hi = [crh1,crh2,crh3,crh4,crh5,crh6,crh7,crh8,crh9,crh10,crh11,crh12]
        
        return [dec_lo, dec_hi, rec_lo, rec_hi]
filter_bank = Bio35FilterBank()
Nwav35bio = pywt.Wavelet(name="myWavelet35", filter_bank=filter_bank)


def Desempaquev3H(numBandas):

    arreBanda= []
    arrefe   = []
    arrerp   = []
    arredesp=[]
    arreesca=[]

    for i in range(numBandas):

        #------------------------------------------------
        #-----------LECTURA DE ARCHIVO DE CABECERA--------
        f2 = open('headerData'+str(i+1)+'.pjt','rb')

        #numeros de bits que se agregaron a los archivos de datos
        NBex = int.from_bytes(f2.read(1), "big")
        Mx   = int.from_bytes(f2.read(1), "big")
        Nx   = int.from_bytes(f2.read(1), "big")
        Fex  = int.from_bytes(f2.read(1), "big")
        Rpx  = int.from_bytes(f2.read(1), "big")
        Despx= int.from_bytes(f2.read(3), "big")
        Escax= int.from_bytes(f2.read(3), "big")
        Sizex= int.from_bytes(f2.read(2), "big")
        TablaSelecx = int.from_bytes(f2.read(1), "big")
        SimboloFaltax = int.from_bytes(f2.read(1), "big")

        #print(NBex,"-",Mx,"-",Nx,"-",Fex,"-",Rpx,"-",Despx,"-",Escax,"-",Sizex,"-",TipoDecox,"-",TablaSelecx,"-",SimboloFaltax)

        f2.close()

        #------------------------------------------------
        #-----------LECTURA DE ARCHIVO DE DATOS--------

        f = open('archivoH'+str(i+1)+'.pjt', 'rb')

        #lectura de todos los bits siguentes
        #se almacenan en beta como beta='10101010101011' sin los 8bits de NBex
        beta = ''
        for j in f.read():
            beta = beta + '{0:08b}'.format(j)

        f.close()
        #-------------------------------------------
        
        #elimina los bits agregados 
        beta = beta[NBex:len(beta)]
        
        #DETERMINA SI SE DECODIFICA CON O SIN HUFFMAN
        #LEE LA CABECERA
        if TablaSelecx == 66: #sin huffman
            #los bits de beta que quedan son de la matriz-subbanda
            #tomar cada rp bits
            Lrp = []
            for val2 in range(int(len(beta)/Rpx)):
                Lrp.append(int(beta[(Rpx*val2):(Rpx*val2+Rpx)],2))

            #Lrp SIN HF / reacondiciona la lista a MATRIZ 2D --SUBBANDA }
            Lrp = np.reshape(Lrp,(Mx,Nx))

        else: #con huffman
            
            TablasComponente = Tablas.SeleccionTablas(i+1,SimboloFaltax) #se eleige el diccionario
   
            DicSele = TablasComponente[TablaSelecx]

            Lrp = DecodificaH(beta,DicSele)  # se decodifica
            #reacondiciona la lista a MATRIZ 2D --SUBBANDA
            print("Cantidad Lrp "+str(i)+": ",len(Lrp))
            if(Mx*Nx==len(Lrp)):
                Lrp = np.reshape(Lrp,(Mx,Nx))

            elif(Mx*Nx>len(Lrp)):
                data_add=np.zeros((1,int(Mx*Nx-len(Lrp))),'uint')
                data_add=data_add.tolist()
                data_add=data_add[0]
                Lrp = Lrp.extend(data_add)
                Lrp = np.reshape(Lrp,(Mx,Nx))

            elif(Mx*Nx<len(Lrp)):
                #data_add=np.zeros((1,int(Mx*Nx-len(Lrp))),'uint')
                #data_add=data_add.tolist()
                #data_add=data_add[0]
                #Lrp = Lrp.extend(data_add)
                Lrp = Lrp[0:Mx*Nx]
                Lrp = np.reshape(Lrp,(Mx,Nx))


        
        
        #
        arreBanda.append(Lrp)
        arrefe.append(Fex)
        arrerp.append(Rpx)

        arredesp.append(Despx/100000)
        arreesca.append(Escax/100000)
    
    return arreBanda,arrefe,arrerp,arredesp,arreesca

def DecodificaH(StringBinario,TablaDic):
    ListaValSubbanda = []

    #valores
    xval = list(TablaDic.keys())
    

    while len(StringBinario)>0:

        for k in range(len(xval)):
            #se obtiene el segmento de bits respecto a valores entero del diccionario 
            Bitsdic = TablaDic[xval[k]]
            #si el valor de bits Bdic es = a los bits del desempaquetado
            #se alamacena el valor entero realcionado al bit en el diccionario
            #tambien se elimina esos bits de la varible reemplazando con vacio
            if Bitsdic == StringBinario[0:len(Bitsdic)]:    
            
                ListaValSubbanda.append(xval[k])
                StringBinario = StringBinario.replace(Bitsdic,'',1)
        #print(len(StringBinario))
    
    return ListaValSubbanda

def Cuantiv3(bcompri,fe,RP,desp,esca):
    
    [F] = np.shape(bcompri)
    
    BandaNivelado = []

    #val_ecu = (fe[0])/(pow(2,(RP[0]-1)))
    #BandaNivelado.append(bcompri[0]*val_ecu)

    for i in range(F):
        paso1=0
        paso2=0
        paso3=0

        val_ecu = (fe[i])/(pow(2,(RP[i]))-1)
        paso1 = (bcompri[i]*val_ecu)/(pow(2,(RP[i]))-1)
        paso2 = paso1 * esca[i]
        paso3 = paso2 - desp[i]
        BandaNivelado.append(paso3)
    
    return BandaNivelado

def Reconstruc(BandaNivelado,wavelet_nor):

    Y3  = pywt.idwt2((BandaNivelado[0],(BandaNivelado[1],BandaNivelado[2],None)),wavelet_nor,'periodization')
    Y2  = pywt.idwt2((Y3,(None,None,None)),wavelet_nor,'periodization')
    Y1  = pywt.idwt2((Y2,(None,None,None)),wavelet_nor,'periodization')

    CB4 = pywt.idwt2((BandaNivelado[3],(None,None,None)),wavelet_nor,'periodization')
    CB3 = pywt.idwt2((CB4,(None,None,None)),wavelet_nor,'periodization')
    CB2 = pywt.idwt2((CB3,(None,None,None)),wavelet_nor,'periodization')
    CB1 = pywt.idwt2((CB2,(None,None,None)),wavelet_nor,'periodization')

    CR4 = pywt.idwt2((BandaNivelado[4],(None,None,None)),wavelet_nor,'periodization')
    CR3 = pywt.idwt2((CR4,(None,None,None)),wavelet_nor,'periodization')
    CR2 = pywt.idwt2((CR3,(None,None,None)),wavelet_nor,'periodization')
    CR1 = pywt.idwt2((CR2,(None,None,None)),wavelet_nor,'periodization')



    return Y1,CB1,CR1

def UpSampling(Y1,CB1,CR1):
    #formato 420
    #upsampling orden 0
    [tf,tc] = Y1.shape
    CBI = np.zeros((tf,tc))
    CRI = np.zeros((tf,tc))

    for f in range(0,ceil(tf/2)):
        for c in range(0,ceil(tc/2)):
            CBI[2*f,2*c] = CB1[f,c]
    
    CBI[:,1::2] = CBI[:,0::2]
    CBI[1::2,:]  = CBI[0::2,:]

    for f in range(0,ceil(tf/2)):
        for c in range(0,ceil(tc/2)):
            CRI[2*f,2*c] = CR1[f,c]
    
    CRI[:,1::2] = CRI[:,0::2]
    CRI[1::2,:]  = CRI[0::2,:]

    return CBI,CRI

def UEntero8(A):
    [f,c]=np.shape(A)

    for i in range(0,f):
        for j in range(0,c):
            if A[i][j]<0:
                A[i][j]=0
            elif A[i][j]>255:
                A[i][j]=255
        
    return A

def convertYtoRGB(Y1,CB1,CR1,Z):

    R1 = 1*Y1 + 1*CB1 - 1*CR1
    G1 = 1*Y1 + 1*CR1
    B1 = 1*Y1 - 1*CB1 - 1*CR1


    R1 = np.round(R1)
    G1 = np.round(G1)
    B1 = np.round(B1)

    R1=UEntero8(R1)
    G1=UEntero8(G1)
    B1=UEntero8(B1)

  
    
    Z[:,:,0]=R1
    Z[:,:,1]=G1
    Z[:,:,2]=B1
    Z=np.uint8(Z)



    return Z
