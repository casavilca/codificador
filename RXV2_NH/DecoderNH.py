from ProcesosDecoderNH import *
import cv2
import matplotlib.pyplot as plt
import numpy as np
import PIL

# ListSizeBanda es una lista de tamaño de en bits [YA,YH,YV,Co,Cg]
[SubBandas,ListFe,ListRp,ListDesp,ListEsca,ListSizeBanda] = Desempaquev3N(5)
BandaNivelado             = Cuantiv3(SubBandas,ListFe,ListRp,ListDesp,ListEsca)

[Y1,CO1,CG1]              = Reconstruc(BandaNivelado,Nwav39bio)#AQUI SE PUEDE CAMBIAR CON Nwav35bio q es bior3.5, solo esta implemntado para 2 filtros 3.9 y 3.5
[COI,CGI]                 = UpSampling(Y1,CO1,CG1)

[M,N] = np.shape(Y1)
G=np.zeros((M,N,3))
Z=convertYtoRGB(Y1,COI,CGI,G)

IMG=cv2.cvtColor(Z,cv2.COLOR_BGR2RGB)
cv2.imwrite("main.jpg", IMG)
cv2.imwrite("main2.png", IMG)

ventana='main'
cv2.namedWindow(ventana, cv2.WINDOW_NORMAL)
cv2.imshow(ventana, IMG)
cv2.waitKey(0) 
cv2.destroyAllWindows() 