import serial
import time
import numpy as np
import random

ser=serial.Serial('COM5')
ser.baudrate = 115200
ser.timeout=0.05
print("PUERTO:", ser.name)


#PARAMETER
time_sleep_Data=2
time_sleep_Header=2
probabilidad=20
payload=144
counter_package_loss=0
counter_max_loss=1




def package_builder(is_header,is_data_image,counter,package,time_sleep):

	pack_struct=is_header+b';'+is_data_image+b';'+bytes([counter])+b';'+bytes(package)	
	print(pack_struct[0:10])
	ser.write(pack_struct)
	print(len(pack_struct))
	time.sleep(time_sleep)


def send_header(archivo):
	#Variables
	is_package_less15=False
	package = []
	BitvalBrain=''

	is_header=b's'
	is_data_image=b's'
	
	counter=0

	ulr='headerData'+str(archivo)+'.pjt' #DIRECCION DEL ARCHIVO DE BITS
	f = open(ulr, 'rb')

	for data in f.read():
		package.append(data)
		BitvalBrain = BitvalBrain + '{0:08b}'.format(data)

	SizeBanda = int(BitvalBrain[88:104],2)
	print("TAMAÑO DE LA BANDA",SizeBanda)
	last_packge = SizeBanda/payload-int(SizeBanda/payload)
	SizeBanda=int(SizeBanda/8)
	#print(SizeBanda)

	print("PAQUETE HEADER:",package)
	print("PAQUETE HEADER:",bytes(package))

	if(last_packge<0.11):
		is_package_less15=True
	#print(is_package_less15)
	package_builder(is_header,is_data_image,archivo,package,time_sleep_Header)
	package_builder(is_header,is_data_image,archivo,package,time_sleep_Header)

	return SizeBanda,is_package_less15

def send_footer():
	is_footer=b's'
	is_data_image=b's'
	is_last_package=b's'
	counter=0
	package=b'fn'

	package_builder(is_footer,is_data_image,counter,package,time_sleep_Header)
	package_builder(is_footer,is_data_image,counter,package,time_sleep_Header)

def isRetransmission():
	re_transmision=ser.read(4)
	try:
		msm=re_transmision.decode()
		if(msm=="reTX"):
			print("MENSAJE RECEPCIONADIO :",msm)
			return True
		else:
			return False
	except:
		return False
		
def send_data():
	print("INCIANDO TRANSMISION")
	is_header=b'n'
	is_data_image=b's'
	is_last_package=b'n'
	ctn_loss_paquete=0

	for archivo in range(1,6):
		#VARIABLES
		package_counter=0
		package = []
		byte_counter = 0

		#RETRANSMISIÓN DE LA IMAGEN

		SizeBanda,is_package_less15 = send_header(archivo)
		
		ulr='archivoH'+str(archivo)+'.pjt' #DIRECCION DEL ARCHIVO DE BITS
		f = open(ulr, 'rb')

		print("============="+ulr+"=========================")

		for data in f.read():
			byte_counter += 1
			package.append(data)

			if(len(package)==payload or byte_counter==SizeBanda):
				aletorio_send=0
				if(counter_package_loss != counter_max_loss):
					#aletorio_send=random.randint(1,probabilidad)
					aletorio_send=0

				print("NUMERO ALEATORIO: ",aletorio_send)
				#print("isRetransmission",isRetransmission())

				if(isRetransmission()==True):
					print("INTENTADO RETRANSMITIR IMAGEN")
					return False

				print("==========",len(package),"=============")
				print(byte_counter,SizeBanda)
				package_counter +=1
				
				if(aletorio_send!=1):
					package_builder(is_header,is_data_image,package_counter,package,time_sleep_Data)
				else:
					ctn_loss_paquete +=1
				if(package_counter==SizeBanda):
					print("ARCHIVO "+str(archivo)+" ENVIADO")
				package=[]

		f.close()
		
	send_footer()
	return True

def run():
	send_ok=True
	while True:

		if(send_ok):
			t_start=time.time()

		send_ok=send_data()

		if(send_ok):
			t_final=time.time()
			print("Tiempo de TX :",int(t_final-t_start))
			print("==========================Sleeping============================")
			break
			#time.sleep(30)
			#print("====I'm ready to work========")

run()