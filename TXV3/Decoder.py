
from ProcesosDecoder import *
import cv2
import matplotlib.pyplot as plt


[arreBanda,arrefe,arrerp,arredesp,arreesca] = Desempaquev3H(5)#desempaquetadon
BandaNivelado             = Cuantiv3(arreBanda,arrefe,arrerp,arredesp,arreesca)
[Y1,CB1,CR1]              = Reconstruc(BandaNivelado,Nwav39bio)
[CBI,CRI]                 = UpSampling(Y1,CB1,CR1)

G=np.zeros((1504,248,3))
Z=convertYtoRGB(Y1,CBI,CRI,G)

IMG=cv2.cvtColor(Z,cv2.COLOR_BGR2RGB)

ventana='main'
cv2.namedWindow(ventana, cv2.WINDOW_NORMAL)
cv2.imshow(ventana, IMG)
cv2.waitKey(0) 
cv2.destroyAllWindows() 