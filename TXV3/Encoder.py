
from ProcesosEncoder import*
import cv2
import time


tic = time.time()

#Irgb = cv2.imread('../Imagenes/imagenA-3-20.jpg')
#Irgb = cv2.imread('../Imagenes/imagenA-6-7.jpg')
#Irgb = cv2.imread('../Imagenes/imagenA-10-2.jpg')
#Irgb = cv2.imread('../Imagenes/imagenB-3-26.jpg')
Irgb = cv2.imread('../Imagenes/ORIGINAL-6-9.jpg')

I1 = cv2.cvtColor(Irgb,cv2.COLOR_BGR2RGB)

[Iy,ICo,ICg]                 = RGBtoYCOCG(I1)
[ICo1,ICg1]                  = SubSampling(ICo,ICg)
[Ya3,YH3,YV3,CoA4,CgA4]      = Descom(Iy,ICo1,ICg1,Nwav39bio)
[bcompri,fe,RP,desp,esca]    = Recuanv3(Ya3,YH3,YV3,CoA4,CgA4,5,4,4)

[bitsOut,bitsBrain]          = Empaquev3H(bcompri,fe,RP,desp,esca)

toc = time.time()

print("Factor de compresion: ",(1504*248*3*8)/bitsOut)
print("Bytes de salida: ",bitsOut/(8000),"KBytes")
print("El tiempo es = ",toc-tic)