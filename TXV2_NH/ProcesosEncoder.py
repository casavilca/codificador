import cv2
import numpy as np
import pywt
from math import ceil
from time import time
import huffman

class Bio39FilterBank(object):
    @property
    def filter_bank(self):

        cdl1 = -0.000239631196178
        cdl2 =  0.000718893588534
        cdl3 =  0.001783921127103
        cdl4 = -0.007268812950735
        cdl5 = -0.004975200073030
        cdl6 =  0.034948117944193
        cdl7 =  0.004336183549889
        cdl8 = -0.112877704409214
        cdl9 =  0.000722697258315
        cdl10=  0.332128837902809
        cdl11=  0.332128837902809
        cdl12=  0.000722697258315
        cdl13= -0.112877704409214
        cdl14=  0.004336183549889
        cdl15=  0.034948117944193
        cdl16= -0.004975200073030
        cdl17= -0.007268812950735
        cdl18=  0.001783921127103
        cdl19=  0.000718893588534
        cdl20= -0.000239631196178


        cdh1  =  0
        cdh2  =  0
        cdh3  =  0
        cdh4  =  0
        cdh5  =  0
        cdh6  =  0
        cdh7  =  0
        cdh8  =  0
        cdh9  = -0.125000000000000
        cdh10 =  0.375000000000000
        cdh11 = -0.375000000000000
        cdh12 =  0.125000000000000
        cdh13 =  0
        cdh14 =  0
        cdh15 =  0
        cdh16 =  0
        cdh17 =  0
        cdh18 =  0
        cdh19 =  0
        cdh20 =  0

        crl1  = 0
        crl2  = 0
        crl3  = 0
        crl4  = 0
        crl5  = 0
        crl6  = 0
        crl7  = 0
        crl8  = 0
        crl9  = 0.501449584960938
        crl10 = 1.504348754882813
        crl11 = 1.504348754882813
        crl12 = 0.501449584960938
        crl13 = 0
        crl14 = 0
        crl15 = 0
        crl16 = 0
        crl17 = 0
        crl18 = 0
        crl19 = 0
        crl20 = 0

        crh1  = -0.000961303710938
        crh2  = -0.002883911132813
        crh3  =  0.007156372070313
        crh4  =  0.029159545898438
        crh5  = -0.019958496093750
        crh6  = -0.140197753906250
        crh7  =  0.017395019531250
        crh8  =  0.452819824218750
        crh9  =  0.002899169921875
        crh10 = -1.332366943359375
        crh11 =  1.332366943359375
        crh12 = -0.002899169921875
        crh13 = -0.452819824218750
        crh14 = -0.017395019531250
        crh15 =  0.140197753906250
        crh16 =  0.019958496093750
        crh17 = -0.029159545898438
        crh18 = -0.007156372070313
        crh19 =  0.002883911132813
        crh20 =  0.000961303710938


        dec_lo = [cdl1,cdl2,cdl3,cdl4,cdl5,cdl6,cdl7,cdl8,cdl9,cdl10,cdl11,cdl12,cdl13,cdl14,cdl15,cdl16,cdl17,cdl18,cdl19,cdl20]
        dec_hi = [cdh1,cdh2,cdh3,cdh4,cdh5,cdh6,cdh7,cdh8,cdh9,cdh10,cdh11,cdh12,cdh13,cdh14,cdh15,cdh16,cdh17,cdh18,cdh19,cdh20]
        rec_lo = [crl1,crl2,crl3,crl4,crl5,crl6,crl7,crl8,crl9,crl10,crl11,crl12,crl13,crl14,crl15,crl16,crl17,crl18,crl19,crl20]
        rec_hi = [crh1,crh2,crh3,crh4,crh5,crh6,crh7,crh8,crh9,crh10,crh11,crh12,crh13,crh14,crh15,crh16,crh17,crh18,crh19,crh20]
        
        return [dec_lo, dec_hi, rec_lo, rec_hi]
filter_bank = Bio39FilterBank()
Nwav39bio = pywt.Wavelet(name="myWavelet39", filter_bank=filter_bank)

class Bio35FilterBank(object):
    @property
    def filter_bank(self):

        cdl1 = -0.0048828125
        cdl2 =  0.0146484375
        cdl3 =  0.0185546875
        cdl4 = -0.0947265625
        cdl5 = -0.0253906250
        cdl6 =  0.3417968750
        cdl7 =  0.3417968750
        cdl8 = -0.0253906250
        cdl9 = -0.0947265625
        cdl10=  0.0185546875
        cdl11=  0.0146484375
        cdl12= -0.0048828125

        cdh1 =  0
        cdh2 =  0
        cdh3 =  0
        cdh4 =  0
        cdh5 = -0.1250
        cdh6 =  0.3750
        cdh7 = -0.3750
        cdh8 =  0.1250
        cdh9 =  0
        cdh10 = 0
        cdh11 = 0
        cdh12 = 0

        crl1 = 0
        crl2 = 0
        crl3 = 0
        crl4 = 0
        crl5 = 0.5
        crl6 = 1.5
        crl7 = 1.5
        crl8 = 0.5
        crl9 = 0
        crl10= 0
        crl11= 0
        crl12= 0

        crh1 =  -0.01953125
        crh2 =  -0.05859375
        crh3 =   0.07421875
        crh4 =   0.37890625
        crh5 =  -0.10156250
        crh6 =  -1.36718750
        crh7 =   1.36718750
        crh8 =   0.10156250
        crh9 =  -0.37890625
        crh10 = -0.07421875
        crh11 =  0.05859375
        crh12 =  0.01953125

        dec_lo = [cdl1,cdl2,cdl3,cdl4,cdl5,cdl6,cdl7,cdl8,cdl9,cdl10,cdl11,cdl12]
        dec_hi = [cdh1,cdh2,cdh3,cdh4,cdh5,cdh6,cdh7,cdh8,cdh9,cdh10,cdh11,cdh12]
        rec_lo = [crl1,crl2,crl3,crl4,crl5,crl6,crl7,crl8,crl9,crl10,crl11,crl12]
        rec_hi = [crh1,crh2,crh3,crh4,crh5,crh6,crh7,crh8,crh9,crh10,crh11,crh12]
        
        return [dec_lo, dec_hi, rec_lo, rec_hi]
filter_bank = Bio35FilterBank()
Nwav35bio = pywt.Wavelet(name="myWavelet35", filter_bank=filter_bank)



def UEntero8(A):
    [f,c]=np.shape(A)

    for i in range(0,f):
        for j in range(0,c):
            if A[i][j]<0:
                A[i][j]=0
            elif A[i][j]>255:
                A[i][j]=255
        
    return A

def RGBtoYCOCG(I1):

    Irgb = np.array(I1,dtype=float)

    R=Irgb[:,:,0]
    G=Irgb[:,:,1]
    B=Irgb[:,:,2]
    
    Iy  =  0.25*R + 0.50*G + 0.25*B
    ICo =  0.50*R - 0.50*B
    ICg = -0.25*R + 0.50*G - 0.25*B

    return Iy,ICo,ICg

def SubSampling(ICo,ICg):

    #round of component ycbcr(CORRECTO)

    #ICo = np.round(ICo)
    #ICg = np.round(ICg)

    #formato 420
    ICo=ICo[::2, ::2]
    ICg=ICg[::2, ::2]
    
    return ICo,ICg

def Descom(Iy,ICo,ICg,wavelet_nor):

    # Y luminancia
    Ya1, (YH1, YV1, YD1) = pywt.dwt2(Iy,wavelet_nor,'periodization')
    Ya2, (YH2, YV2, YD2) = pywt.dwt2(Ya1,wavelet_nor,'periodization')
    Ya3, (YH3, YV3, YD3) = pywt.dwt2(Ya2,wavelet_nor,'periodization')
    # Co luminancia
    CoA1, (CoH1, CoV1, CoD1) = pywt.dwt2(ICo,wavelet_nor,'periodization')
    CoA2, (CoH2, CoV2, CoD2) = pywt.dwt2(CoA1,wavelet_nor,'periodization')
    CoA3, (CoH3, CoV3, CoD3) = pywt.dwt2(CoA2,wavelet_nor,'periodization')
    CoA4, (CoH4, CoV4, CoD4) = pywt.dwt2(CoA3,wavelet_nor,'periodization')
    # Cg luminancia
    CgA1, (CgH1, CgV1, CgD1) = pywt.dwt2(ICg,wavelet_nor,'periodization')
    CgA2, (CgH2, CgV2, CgD2) = pywt.dwt2(CgA1,wavelet_nor,'periodization')
    CgA3, (CgH3, CgV3, CgD3) = pywt.dwt2(CgA2,wavelet_nor,'periodization')
    CgA4, (CgH4, CgV4, CgD4) = pywt.dwt2(CgA3,wavelet_nor,'periodization')

    return Ya3,YH3,YV3,CoA4,CgA4

def Recuanv3(YA,YH3,YV3,CAo,CAg,rpy,rpb,rpr):

    rpycbcr= [rpy,rpy,rpy,rpb,rpr]
    bandas = [YA,YH3,YV3,CAo,CAg]
    [F] = np.shape(bandas)
    
    RP=[]
    fe=[]
    bcompri=[]

    desp=[]
    esca=[]

    
    for i in range(F):
        
        desp.append(abs(np.min(bandas[i])))

        desplaza = abs(np.min(bandas[i]))+bandas[i]

        esca.append(np.max(desplaza))

        escala = (desplaza/np.max(desplaza))*(pow(2,(rpycbcr[i]))-1)
        #escala   = (desplaza*pow(2,(rpycbcr[i]))-1)/np.max(desplaza)

        fe.append(np.max(np.abs(escala)))
        
        #aprox lumininacia
        RP.append(rpycbcr[i])
        val_ecu=(pow(2,(rpycbcr[i]))-1)/fe[i]
        bcompri.append(np.round(escala*val_ecu))
    print("fe :",fe)
    return bcompri,fe,RP,desp,esca


#crea el diccionario y agrupa los bits a enviar
def GrupBitsDatos(subM,fex,rpx,desp_val,esca_val):
    #armar LOS BITS
    [fila,col] = np.shape(subM)
    #matriz
    subM = subM.astype(int)
    #array 1 fila
    subM = subM.flatten()

    #convert all, integer to one  string-bits
    Bagrup = ''
    for val in subM:
        Bagrup = Bagrup + (('{0:0'+str(rpx)+'b}').format(val))
    
    #si es divisible entre 8 no se agrega una cabezera de bits para completar
    #los bits al inicio siempre se agregan asi sea divisible o no
    if len(Bagrup)%8 == 0:
        NBitsFaltan = 0
        print('es divisible --  no se completo bits')
    
    else:
        NBitsFaltan = (8*((len(Bagrup)//8)+1) - len(Bagrup))
        print('Se completo con= ',NBitsFaltan," bits")

        BitsFaltan = ''
        for agrega in range(NBitsFaltan):
            BitsFaltan = BitsFaltan + '1'
        
        Bagrup = BitsFaltan + Bagrup


    #se guarda el tamaño de bits agrupados total para mostrar
    TamBagroup = len(Bagrup)
    #print("Tamño de bits: ",TamBagroup)
    #tomar 8 en 8 and convert to int de los bits agrupados
    #se guardan todos los enteros en una lista LintuH
    LintuH = []
    for val2 in range(int(len(Bagrup)/8)):
        #se toma cada 8bits strings y se convierte a entero con base 2
        #ejemplo '00000101' = 5, estos valores se guardan en una lista 
        LintuH.append(int(Bagrup[8*val2:8*val2+8],2))
    
            
    return LintuH,TamBagroup,fila,col,NBitsFaltan

def GrupBitsCabecera(SizeBx,filax,colx,NBitsAdd,fex,rpx,desp_val,esca_val):

    
    NBits = '{0:08b}'.format(NBitsAdd)
    Mx    = '{0:08b}'.format(filax)
    Nx    = '{0:08b}'.format(colx)
    fex   = '{0:08b}'.format(fex)
    rpx   = '{0:08b}'.format(rpx)
    despx = '{0:024b}'.format(desp_val)
    escax = '{0:024b}'.format(esca_val)
    SizeB = '{0:016b}'.format(SizeBx)
    

    BagrupBrain =  NBits + Mx + Nx + fex + rpx + despx + escax + SizeB

    TamBagroupBrain = len(BagrupBrain)

    ListaBrain = []
    for valbrain in range(int(len(BagrupBrain)/8)):
        ListaBrain.append(int(BagrupBrain[8*valbrain:8*valbrain+8],2))
    print("Cabecera :",ListaBrain)
    return ListaBrain,TamBagroupBrain



def Empaquev3N(bcompri,fe,RP,despLi,escaLi):
    
    despLi = np.round(np.array(despLi)*100000)
    escaLi = np.round(np.array(escaLi)*100000)

    despLi = np.array(despLi,dtype=int)
    escaLi = np.array(escaLi,dtype=int)
    
    bitsOut = 0
    bitsBrain = 0
    Dic =[]
    for i in range(len(bcompri)):
        #----------------------------------------------
        #----------------------------------------------
        #----------------------------------------------
        f = open('archivoNH'+str(i+1)+'.pjt', 'wb')

        [LintuH,TamBagroup,fila,col,NBitsFaltan] = GrupBitsDatos(bcompri[i], int(round(fe[i])), RP[i], despLi[i], escaLi[i])
        #print(TamBagroup)
        f.write(bytearray(LintuH))# matriz
        f.close()

        bitsOut = bitsOut + TamBagroup

        f2 = open('headerData'+str(i+1)+'.pjt','wb')
        
        [ListaBrain,TamBrain] = GrupBitsCabecera(TamBagroup,fila,col,NBitsFaltan,int(round(fe[i])), RP[i], despLi[i], escaLi[i])
        
        f2.write(bytearray(ListaBrain))# matriz
        f2.close()

        bitsBrain = bitsBrain + TamBrain
        print("El tamaño= ",TamBagroup)
    
    return bitsOut,bitsBrain
 