from ProcesosEncoder import *
import cv2
import matplotlib.pyplot as plt
import numpy as np
import PIL

#Irgb = cv2.imread('../Imagenes/imagenA-3-20.jpg')
#Irgb = cv2.imread('../Imagenes/imagenA-6-7.jpg')
#Irgb = cv2.imread('../Imagenes/imagenA-10-2.jpg')
#Irgb = cv2.imread('../Imagenes/imagenB-3-26.jpg')
Irgb = cv2.imread('../Imagenes/imagenB-6-19.jpg')

I1 = cv2.cvtColor(Irgb,cv2.COLOR_BGR2RGB)

[Iy,ICo,ICg]                 = RGBtoYCOCG(I1)
[ICo1,ICg1]                  = SubSampling(ICo,ICg)
[Ya3,YH3,YV3,CoA4,CgA4]      = Descom(Iy,ICo1,ICg1,Nwav39bio)
[bcompri,fe,RP,desp,esca]    = Recuanv3(Ya3,YH3,YV3,CoA4,CgA4,5,4,4)



[bitsOut,bitsBrain]          = Empaquev3N(bcompri,fe,RP,desp,esca)

print("Factor de compresion: ",(1504*248*3*8)/bitsOut)
print("Bits totales de Dato : ",bitsOut/8000,"KBytes")
print("Bits totales de Cabezera: ",bitsBrain," bits")
