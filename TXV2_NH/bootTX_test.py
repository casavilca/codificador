import random
import serial
import time
import numpy as np


ser=serial.Serial('COM7')
ser.baudrate = 115200

print("PUERTO:", ser.name)


#PARAMETER
time_sleep_Data=1.5
time_sleep_Header=1.5
ctn_loss_paquete=0
probabilidad=100000
payload=144




def package_noHuffman(is_header,is_data_image,counter,package,time_sleep):

	
	pack_struct=is_header+b';'+is_data_image+b';'+bytes([counter])+b';'+bytes(package)	

	print(pack_struct[0:10])

	ser.write(pack_struct)
	print(len(pack_struct))
	time.sleep(time_sleep)

def fin_archivo():
	send_data('fin'.encode())
	send_data('fin'.encode())

def fin_transmision():
	send_data('fin tx'.encode())
	send_data('fin tx'.encode())

def send_header(archivo):
	#Variables
	is_package_less15=False
	package = []
	BitvalBrain=''
	is_header=b's'
	is_data_image=b's'
	is_last_package=b's'
	counter=0

	ulr='headerData'+str(archivo)+'.pjt' #DIRECCION DEL ARCHIVO DE BITS
	f = open(ulr, 'rb')

	for data in f.read():
		package.append(data)
		BitvalBrain = BitvalBrain + '{0:08b}'.format(data)

	SizeBanda = int(BitvalBrain[88:104],2)
	last_packge = SizeBanda/payload-int(SizeBanda/payload)
	SizeBanda=int(SizeBanda/8)
	#print(SizeBanda)
	print("PAQUETE HEADER:",package)
	print("PAQUETE HEADER:",bytes(package))

	if(last_packge<0.11):
		is_package_less15=True
	#print(is_package_less15)
	package_noHuffman(is_header,is_data_image,counter,package,time_sleep_Header)
	package_noHuffman(is_header,is_data_image,counter,package,time_sleep_Header)

	return SizeBanda,is_package_less15

def send_footer():
	is_footer=b's'
	is_data_image=b's'
	is_last_package=b's'
	counter=0
	package=b'footer'

	package_noHuffman(is_footer,is_data_image,counter,package,time_sleep_Header)
	package_noHuffman(is_footer,is_data_image,counter,package,time_sleep_Header)
	
def send_noHuffman():
	
	is_header=b'n'
	is_data_image=b's'
	is_last_package=b'n'
	ctn_loss_paquete=0

	for archivo in range(1,6):
		#VARIABLES
		counter_aleatorio=0 #PERTECENE A PRUEBAS
		package_counter=0
		package = []
		byte_counter = 0

		SizeBanda,is_package_less15 = send_header(archivo)

		ulr='archivoNH'+str(archivo)+'.pjt' #DIRECCION DEL ARCHIVO DE BITS
		f = open(ulr, 'rb')

		print("============="+ulr+"=========================")

		for data in f.read():
			byte_counter += 1
			package.append(data)

			#OBTENIEDO UN NUMERO ALEATORIO
			if(len(package)==payload or byte_counter==SizeBanda):
				#aletorio_send=random.randint(1,probabilidad)
				aletorio_send=0
				print("NUMERO ALEATORIO: ",aletorio_send)
				package_counter +=1

				if(aletorio_send!=1):	

					print("==========",len(package),"=============")
					print(byte_counter,SizeBanda)
					package_noHuffman(is_header,is_data_image,package_counter,package,time_sleep_Data)

					print("CONTADOR DE PAQUETES",package_counter)
					print("BYTES COUNTER",byte_counter)
				else:
					ctn_loss_paquete += 1
					print("PAQUETE PERDIDO")

				if(package_counter==SizeBanda):
					print("ARCHIVO "+str(archivo)+" ENVIADO")
				package=[]



		f.close()	
	send_footer()
	return ctn_loss_paquete
while True:
#     print("INCIANDO TX")
#     start=time.time()
#     take_photo()
#     fin=time.time()
#     print("TIEMPO CAPTURA: ",fin-start)
#     try:
#         start=time.time()
#         os.system('python3 EncoderNH.py')
#         fin=time.time()
#         print("TIEMPO EJECUCIÓN ALGORITMO: ",fin-start)
#     except Exception as e:
#         print("Error en el Encoder, reiniciando proceso")
#         print(e)
#         continue
    start=time.time()
    send_noHuffman()
    fin=time.time()
    print("TIEMPO DE TRANSMISION: ",fin-start)
    print("Sleep TX")
    time.sleep(25)