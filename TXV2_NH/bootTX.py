import serial
import time
import numpy as np

ser=serial.Serial('COM5')
ser.baudrate = 115200

print("PUERTO:", ser.name)


#PARAMETER
time_sleep_Data=1.5
time_sleep_Header=1.5
payload=144




def package_noHuffman(is_header,is_data_image,counter,package,time_sleep):

	
	pack_struct=is_header+b';'+is_data_image+b';'+bytes([counter])+b';'+bytes(package)	

	print(pack_struct[0:10])

	ser.write(pack_struct)
	print(len(pack_struct))
	time.sleep(time_sleep)

def fin_archivo():
	send_data('fin'.encode())
	send_data('fin'.encode())

def fin_transmision():
	send_data('fin tx'.encode())
	send_data('fin tx'.encode())

def send_header(archivo):
	#Variables
	is_package_less15=False
	package = []
	BitvalBrain=''
	is_header=b's'
	is_data_image=b's'
	is_last_package=b's'
	counter=0

	ulr='headerData'+str(archivo)+'.pjt' #DIRECCION DEL ARCHIVO DE BITS
	f = open(ulr, 'rb')

	for data in f.read():
		package.append(data)
		BitvalBrain = BitvalBrain + '{0:08b}'.format(data)

	SizeBanda = int(BitvalBrain[88:104],2)
	last_packge = SizeBanda/payload-int(SizeBanda/payload)
	SizeBanda=int(SizeBanda/8)
	#print(SizeBanda)
	print("PAQUETE HEADER:",package)
	print("PAQUETE HEADER:",bytes(package))

	if(last_packge<0.11):
		is_package_less15=True
	#print(is_package_less15)
	package_noHuffman(is_header,is_data_image,counter,package,time_sleep_Header)
	package_noHuffman(is_header,is_data_image,counter,package,time_sleep_Header)

	return SizeBanda,is_package_less15

def send_footer():
	is_footer=b's'
	is_data_image=b's'
	is_last_package=b's'
	counter=0
	package=b'footer'

	package_noHuffman(is_footer,is_data_image,counter,package,time_sleep_Header)
	package_noHuffman(is_footer,is_data_image,counter,package,time_sleep_Header)
	
def send_noHuffman():
	is_header=b'n'
	is_data_image=b's'
	is_last_package=b'n'

	for archivo in range(1,6):
		#VARIABLES
		package_counter=0
		package = []
		byte_counter = 0

		SizeBanda,is_package_less15 = send_header(archivo)

		ulr='archivoNH'+str(archivo)+'.pjt' #DIRECCION DEL ARCHIVO DE BITS
		f = open(ulr, 'rb')

		print("============="+ulr+"=========================")

		for data in f.read():
			byte_counter += 1
			package.append(data)

			if(len(package)==payload or byte_counter==SizeBanda):
				print("==========",len(package),"=============")
				print(byte_counter,SizeBanda)
				package_counter +=1
				package_noHuffman(is_header,is_data_image,package_counter,package,time_sleep_Data)
				if(package_counter==SizeBanda):
					print("ARCHIVO "+str(archivo)+" ENVIADO")
				package=[]

		f.close()
		
	send_footer()

send_noHuffman()

print("Transmision terminada")